# -*- coding: utf-8 -*-

# Model

# LoadJPKTextFiles

#--------------------Modules to import to run LoadJPKTextFiles-----------------#
import glob
import numpy as np
import scipy as sp
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import pandas as pd
import os
import time
import re
import seaborn as sns
sns.set(style="white")
import sys
if sys.version_info[0] < 3:
    from io import StringIO
else:
    from io import StringIO
from threading import Thread, RLock

# ---------------------------------------------------
# This is a variable arbitrary choosen by the user for the linear fit. So we set here its value.
initialguesslin = [10**(9), 10**3]


#--------------------Functions of LoadJPKTextFiles-----------------------------#

# Young modulus calculation

def ValYoung(p, eta, model):
    """ Young modulus calculation """
    if model == "sphere":  # Hertz, 1881
        young = np.round(p*(10**6)*3.*(1.-eta**2) /
                         (4.*np.sqrt(1000.*Rb)), decimals=1)  # radius in nm
        return young

    if model == "linear":  # krieg, 2009
        slope = p  # (pN/nm)
        return slope


# Retourner function

def Retourner(x, *p):
    """Change the sign of values from the subdf.dataframe in order to have
    +X/-X and +Y/-Y values with the same sign"""

    sortie = -x
    return sortie

# GetInfoFileName:


def GetInfoFileName(x, *p):
    """Take: 1 element (= 1 curve.txt) from files list
     Return: dict[nom_complet, direction, bille, cellule, couple, heure, date]"""
    cut = x.split('-')
    couple = cut[0]
    lshort = (len(couple)-1)
    direction = couple[lshort]
    couple2 = couple.replace(direction, '')
    inter = couple2.replace('b', '')
    if "c" in inter:
        # Source d'erreur car il y a aussi des 'C' dans liste files
        nombres = inter.split('c')
    else:
        nombres = inter.split('C')

    bille = nombres[0]
    cellule = nombres[1]
    heurejpk = cut[2]
    date = cut[1]
    heure = heurejpk.replace('.txt', '')
#    if direction=='g' or direction=='d':
#        side='x'
#    elif direction=='h' or direction =='b':
#        side='y'
    output = {}
    output['nomcomplet'] = x
    output['direction'] = direction
#    output['side']=side
    output['bille'] = int(bille)
    output['cellule'] = int(cellule)
    output['couple'] = couple
    output['heure'] = heure
    output['date'] = date
    return output
#print("output_GetInfoFileName: ", GetInfoFileName(files[1]))

# Tronconner


def Tronconner(x, *p):
    """(Parser) Take: 1 element (= 1 curve.txt) from files list
    Return: 1 curve.txt splitted by segments: headers -> values / headers -> values…"""
    source = open(x, 'r')
    data = source.read()
    troncons = data.split('\n\n')
    source.close()
    return troncons

# RecupereDataFromHeaderAller: Fonction qui récupère un élement de la liste
# découpage, donc soit courbe aller, retour, contact et extrait les paramètres de l’acquisition.


def RecupereDataFromHeaderAller(x, nbsegments, *p):
    """Take: 1 element of decoupage list Return: dict[parameters]"""
    #head = [next(x) for i in xrange(161)]
    output = {}
    for line in x:
        if nbsegments == 1:
            output['tstop'] = 0.0
            output['lretour'] = 0.0
            output['tretour'] = 0.0

        if nbsegments == 2:
            output['tstop'] = 0.0
            if line.startswith("# settings.segment.1.length:"):
                output['lretour'] = round(float(line.split("# settings.segment.1.length:")[
                                          1])*10**6, 1)  # µm sinon ca donne x.9999
            else:
                output['lretour'] = 0.0
            if line.startswith("# settings.segment.1.duration"):
                output['tretour'] = float(line.split(
                    "# settings.segment.1.duration:")[1])

        if nbsegments > 2:
            if line.startswith("# settings.segment.1.duration:"):
                output['tstop'] = float(line.split(
                    "# settings.segment.1.duration:")[1])
            if line.startswith("# settings.segment.2.length:"):
                output['lretour'] = round(float(line.split("# settings.segment.2.length:")[
                                          1])*10**6, 1)  # µm sinon ca donne x.9999
            if line.startswith("# settings.segment.2.duration"):
                output['tretour'] = float(line.split(
                    "# settings.segment.2.duration:")[1])

        if line.startswith("# settings.segments.size:"):
            output['segments_size'] = float(
                line.split("# settings.segments.size:")[1])
        if line.startswith("# settings.segment.0.direction.theta:"):
            output['theta'] = float(line.split(
                "# settings.segment.0.direction.theta:")[1])
        if line.startswith("# settings.segment.0.direction.phi:"):
            output['phi'] = float(line.split(
                "# settings.segment.0.direction.phi:")[1])
        if line.startswith("# settings.segment.0.length:"):
            output['laller'] = round(float(line.split("# settings.segment.0.length:")[
                                     1])*10**6, 1)  # µm sinon ca donne x.9999
        if line.startswith("# settings.segment.0.setpoint.value:"):
            output['fcontact'] = float(line.split(
                "# settings.segment.0.setpoint.value:")[1])
        if line.startswith("# settings.segment.0.xy-scanner.scanner:"):
            # attention a l'espace apres
            output['scanner'] = line.split(
                "# settings.segment.0.xy-scanner.scanner: ")[1]
        if line.startswith("# settings.segment.0.duration"):
            output['taller'] = float(line.split(
                "# settings.segment.0.duration:")[1])

        if line.startswith("# xSignal1_sensitivity:"):
            intermediaire = line.split("# xSignal1_sensitivity:")[1]
            output['sx'] = float(intermediaire.split()[0])*10**9  # nm/V
        if line.startswith("# xSignal1_stiffness:"):
            intermediaire = line.split("# xSignal1_stiffness:")[1]
            output['kx'] = float(intermediaire.split()[0])*10**3  # pN/nm
        if line.startswith("# ySignal1_sensitivity:"):
            intermediaire = line.split("# ySignal1_sensitivity:")[1]
            output['sy'] = float(intermediaire.split()[0])*10**9  # nm/V
        if line.startswith("# ySignal1_stiffness:"):
            intermediaire = line.split("# ySignal1_stiffness:")[1]
            output['ky'] = float(intermediaire.split()[0])*10**3  # pN/nm
        if line.startswith("# zSignal1_sensitivity:"):
            intermediaire = line.split("# zSignal1_sensitivity:")[1]
            output['sz'] = float(intermediaire.split()[0])*10**9  # nm/V
        if line.startswith("# zSignal1_stiffness:"):
            intermediaire = line.split("# zSignal1_stiffness:")[1]
            output['kz'] = float(intermediaire.split()[0])*10**3  # pN/nm
        if line.startswith("# fancyNames:"):
            noms = line.split("# fancyNames: ")[1].replace(
                "\" \"", "\",\"").replace("\"", "")
            # coupe apres fancyNames, recupere la suite, remplace les espaces par des virgules, puis les " par rien
            # cree la liste des noms correctement separes
            listenoms = noms.split(',')
            output['noms'] = listenoms
    return output


# RecoverDirectionFromAngle: Fonction qui retrouve la direction du mouvement en
# fonction de l’angle phi. Elle prend les paramètres “# settings.segment.0.direction.phi:”
# et “# settings.segment.0.xy-scanner.scanner:” qui sont dans le header de courbe.txt

def RecoverDirectionFromAngle(scanner, angle, *p):
    """Take: scanner and phi parameter Return: direction related to phi angle
    with an interval of -pi +/- epsilon"""
    pi = np.pi
    epsilon = 0.01
    # la convention pour +/- est : ou est la cellule par rapport a la bille
    # EXPLICITER LA CONVENTION ICI
    if scanner == 'sample-scanner':
        if (-pi-epsilon) < angle < (-pi+epsilon):
            return "-X"
        elif (-epsilon) < angle < (epsilon):
            return "+X"
        elif (-pi/2.-epsilon) < angle < (-pi/2.+epsilon):
            return "-Y"
        elif (pi/2.-epsilon) < angle < (pi/2.+epsilon):
            return "+Y"
        else:
            return "Angle error"
    else:
        return "Erreur sur le scanner"


# QuelSegment: Fonction qui permet de sélectionner la bonne partie du découpage
# de courbe.txt, suivant que l’on veut traiter de la courbe aller, retour, contact, stop..

def QuelSegment(x, y, *p):
    """Take: x = number of segments obtained after decoupage, y = moment of
    acquisition (‘trace’, ‘retrace’, ‘contact’, ‘stop’) Return: lequel number
    which is the ‘lequel’ elements in decoupage list"""
    if x == 2:
        if y == 'trace':
            lequel = 0
        elif y == 'retrace':
            lequel = 1
    elif x == 3:
        if y == 'trace':
            lequel = 0
        elif y == 'contact':
            lequel = 1
        elif y == 'retrace':
            lequel = 2
    elif x == 4:
        if y == 'trace':
            lequel = 0
        elif y == 'contact':
            lequel = 1
        elif y == 'retrace':
            lequel = 2
        elif y == 'stop':
            lequel = 3
    return lequel


def Get_method_parameters(outputpath, now, load_path=None):
    """Retrieve the parameters contained in a method file.
    Take as arguments:
    - outputpath = of the method file just written after clicking on <Proceed> button on the Starting window of the GUI.
    - now = <now> global variable set in the GUI script
    - load_path = path of the method file loaded after clicking on <Load method file> in the Starting window of the GUI.
    """

    # Check if a method file was loaded or not
    if load_path == None:
        methode = open(outputpath + '/' + now + '-method.txt', 'r')
    else:
        methode = open(load_path, 'r')

    # avoid the \n at the end of the file
    methodelines = methode.read().splitlines()
    #print("methodelines: ", methodelines)
    methode.close()

    # where are the files?
    data_folder = methodelines[1]
    #print("inputpath: ", inputpath)

    # condition
    condition = methodelines[3]
    #print("molecule: ", molecule)

    # model name : sphere, linear
    model = methodelines[5]
    #print("model :", model)

    # SPHERE : radius of bead in um
    Rb = float(methodelines[7])
    #print("Rb: ", Rb)

    # FACTOR to overcome noise (careful it is a real)
    factor = float(methodelines[9])
    #print("factor: ", factor)

    eta = float(methodelines[11])
    epsilon = float(methodelines[14])
    pulling_ratio = float(methodelines[16])
    jump_force = float(methodelines[18])
    jump_position = float(methodelines[20])
    slope_before_jump = float(methodelines[22])
    fitting_frame = int(methodelines[24])

    # Retrieve the last file index
    if '# Incomplete classification - last indexes' in methodelines:
        last_index = methodelines[-1]
        last_index = last_index.strip('[')
        last_index = last_index.strip(']')
        last_index = last_index.split(', ')
        for i in range(0, len(last_index)):
            last_index[i] = int(last_index[i])

    else:
        last_index = None

    return({'data_folder': data_folder, 'condition': condition, 'model': model,
            'bead_radius': Rb, 'noise_factor': factor, 'eta': eta, 'epsilon': epsilon,
            'pulling_ratio': pulling_ratio, 'jump_force': jump_force,
            'jump_position': jump_position, 'slope_before_jump': slope_before_jump,
            'fitting_frame': fitting_frame, 'last_index': last_index})


def Courbe_aller(fichier, inputpath, model, factor, eta, jump_force,
                 jump_position, slope_before_jump, alignment_epsilon, condition, results, list_press, list_pull):
    """Return a dictionnary with all the needed variables to draw the press curves with matplotlib and perform the classification by filling the results dictionnary.
    Takes as arguments:
    - fichier = the name of the processed file
    - results = dictionnary from which will be written the results.txt output file

    The rest are the parameters entered by the user in the Starting window of the GUI
    - inputpath = the path of the data folder
    - model = the name of the used model
    - factor = the factor to overcome noise
    - eta = the compressibilty constant
    - jump_force
    - jump_position
    - slope_before_jump
    - alignment_epsilon
    - condition
    """

    # Fitting functions:
    def Fit(x, *p):
        """ Calculation of the fit related to the choosen model (linear or sphere)"""
        fit = np.ones(len(x))
        j = 0

        if model == "sphere":

            for i in x:
                if i >= p[1]:
                    # fit[j]=p[2]+p[0]*(i-p[1]+p[2]/k)**(3./2)
                    fit[j] = p[2]+p[0]*(i-p[1])**(3./2)
                else:
                    fit[j] = p[2]
                j = j+1
            return fit

        elif model == "linear":

            for i in x:
                if i >= p[1]:
                    # fit[j]=p[2]+p[0]*(i-p[1]+p[2]/k)
                    fit[j] = p[2]+p[0]*(i-p[1])
                else:
                    fit[j] = p[2]
                j = j+1
            return fit

        else:
            print("No fitting")

    who = inputpath + '/' + fichier

    morceau = 'trace'

    decoupage = Tronconner(who)
    #[print("decoupage: ", d[1:10000]) for d in decoupage]

    segments = len(decoupage)
    #print("segments: ", segments)

    # update 26/07/18: ce <if> a été déplacé ici car il y avait une erreur pour ls fichier contenant qu'un seul segment.
    if segments > 1:  # au cas ou la courbe ait un probleme ie arretee a l'aller

        # determiner quel segment on veut
        # recupere les noms des colonnes au cas ou l'export ait ete different de toutes les infos
        chaine = decoupage[QuelSegment(segments, morceau)]
        #print("chaine: ", chaine[1:10000])
        #print("len_chaine: ", len(chaine))

        lignes = chaine.splitlines()
        #print("lignes: ", lignes[1:200])

        # n'est-ce pas la même chose que la variable $lines ?
        aller = decoupage[0].splitlines()
        #print("aller: ", aller[1:200])

        infos = GetInfoFileName(fichier)

        parameters = RecupereDataFromHeaderAller(aller, segments)
        #print("parameters: ", parameters)

        # CLASSIFICATION - if the condition is true, this file is classified as 'IN' = incomplete event. And the courbe_aller() function stops here by returning 'NaN'.
        if incomplete_curves1(nb_segments=segments, header_segments=parameters['segments_size'], tstop=parameters['tstop']) == True:
            for x in results.keys():
                results[x].append('NaN')

            results['Filename'][-1] = infos['nomcomplet']
            results['Direction'][-1] = infos['direction']
            results['Bead'][-1] = infos['bille']
            results['Couple'][-1] = infos['couple']
            results['Hour'][-1] = infos['heure']
            results['Date'][-1] = infos['date']
            results['Cell'][-1] = infos['cellule']
            results['Condition'][-1] = condition
            results['Type of event'][-1] = 'IN'
            results['Type of event (corrected)'][-1] = 'IN'
            print("INCOMPLET")
            return('NaN')

        print("=======================================================")
        print("Source : ", fichier)
        print("----------------------------")
        print("Condition = ", condition)
    #    print "----------------------------"
    #    print "Filename Infos :"
    #    print GetInfoFileName(fichier)

        print("----------------------------")
        print("Nombre de segments = ", segments)
        print("----------------------------")

        print("OK for processing")
        print("----------------------------")
        # ============== ALLER =================

        print("Direction :", RecoverDirectionFromAngle(
            parameters['scanner'], parameters['phi']))
        direction = RecoverDirectionFromAngle(
            parameters['scanner'], parameters['phi'])
        print("----------------------------")

        # transforme la chaine en csv
        interim = StringIO(chaine)
        # print("interim: ", interim) # objet csv -> read.csv()
        #print("interim: ", interim.read(10000))

        # recupere la liste des noms de colonnes
        listenoms = parameters['noms']
        #print("listenoms: ", listenoms)

        # cree le dataframe correspondant au morceau voulu
        subdf = pd.read_csv(interim, delimiter=r"\s+",
                            comment='#', names=listenoms)
        # print("subdf: ", subdf.head(n=10)) # utilisation du module pandas

        # fig1=plt.figure(morceau, figsize=(5, 5), dpi=100) # creation d'une fenetre
        # matplotlib graphique vide
        # plt.show()

        microns = subdf['Displacement']*10**6
        #print("converion_deplacement_microns: ", microns[1:10], microns[-10:-1])

        # retournement "à l'afm" et choix du y et de la constante
        if direction == '+X':
            y = subdf['X Signal 1']*10**12  # pN
            k = parameters['kx']  # pN/nm
        elif direction == '-X':
            y = Retourner(subdf['X Signal 1']*10**12)  # pN
            k = parameters['kx']  # pN/nm
        elif direction == '+Y':
            y = subdf['Y Signal 1']*10**12  # pN
            k = parameters['ky']  # pN/nm
        elif direction == '-Y':
            y = Retourner(subdf['Y Signal 1']*10**12)  # pN
            k = parameters['ky']  # pN/nm

        # pour ramener la force a zero pour l'aller avec juste une moyenne sur 1000 pts
        premierspoints = y[0:100]
        y0 = premierspoints.mean()
        # print("baseline: ", y0) # moyenne des 1000 premiers points

        # CLASSIFICATION - if the condition is true, this file is classified as 'AL' = alignment event. And the courbe_aller() function stops here by returning 'NaN'.
        if optical_alignment_curves(header_fcontact=parameters['fcontact'], moy_premiers_points=y0, direction=direction, dataframe=subdf, epsilon=alignment_epsilon) == True:
            if not results['Sens'][-1] == 'NaN':
                for x in results.keys():
                    results[x].append('NaN')

            results['Filename'][-1] = infos['nomcomplet']
            results['Direction'][-1] = infos['direction']
            results['Bead'][-1] = infos['bille']
            results['Couple'][-1] = infos['couple']
            results['Hour'][-1] = infos['heure']
            results['Date'][-1] = infos['date']
            results['Cell'][-1] = infos['cellule']
            results['Condition'][-1] = condition
            results['Type of event'][-1] = 'AL'
            results['Type of event (corrected)'][-1] = 'AL'
            print("REJETEE!")
            return('NaN')

        piconewtons = y-y0
        #print("piconewtons: ", piconewtons[1:10], piconewtons[-10:-1])
        TSS = 1000*microns - piconewtons / k  # k est en pN/nm
        #print("TSS: ", TSS[1:10], TSS[-10:-1])
        xcorrige = np.abs(TSS-TSS[0])
        #print("x_corrigé: ", xcorrige[1:10], xcorrige[-10:-1])

        # pour verification du sens de la correction tss
        xnoncorrige = np.abs(1000*(microns-microns[0]))
        #print("x_non_corrigé: ", xnoncorrige[1:10], xnoncorrige[-10:-1])

        if (model == 'linear') or (model == 'sphere'):
            if model == "sphere":
                initialguess = [10**2, 10**3, 1]

            elif model == "linear":
                initialguess = [10**(9), 10**3, 1]

            try:
                fitParams, fitCovariances = curve_fit(
                    Fit, xcorrige, piconewtons, p0=initialguess)
                #print("fitParams: ", fitParams)
                #print("fitCovariances: ", fitCovariances, "len_fitCovariances: ", len(fitCovariances))
                fitted = Fit(
                    xcorrige, fitParams[0], fitParams[1], fitParams[2])
                #print("fitted: ", fitted[1:10], "len_fitted: ", len(fitted))

            except TypeError:
                print('Fit failed !')
                fitted = 'fit failed'

            if np.isinf(np.sum(fitCovariances)):
                error = errorYoung = "NaN"
                Young = ValYoung(fitParams[0], eta, model)
                contact = np.round(fitParams[1], decimals=1)
                #print("contact: ", contact)
                errorcontact = "NaN"
            else:
                error = np.sqrt(np.diag(fitCovariances))
                Young = ValYoung(fitParams[0], eta, model)
                errorYoung = ValYoung(error[0], eta, model)
                contact = np.round(fitParams[1], decimals=1)
                #print("contact_else: ", contact)
                errorcontact = np.round(error[1], decimals=1)

        elif model == 'none':
            Young = 'NaN'
            errorYoung = 'NaN'

        else:
            print("Model error")

        # ---------------------------------------------------

        # because some fits are not physical
        if Young < 0.0:
            Young = 'NaN'
            errorYoung = 'NaN'

        # output to command line
        if model == "sphere":
            print("Young modulus (Pa) = ", Young, " +/-", errorYoung)
        elif model == "linear":
            print("Slope (pN/nm) = ", Young, " +/-", errorYoung)
        else:
            print("Trace not processed")

        print("----------------------------")

        try:
            vitesse = round(parameters['lretour']/parameters['tretour'], 1)

        except ZeroDivisionError:
            vitesse = 'NaN'

        results['Filename'].append(infos['nomcomplet'])
        results['Direction'].append(infos['direction'])
        results['Bead'].append(infos['bille'])
        results['Couple'].append(infos['couple'])
        results['Hour'].append(infos['heure'])
        results['Date'].append(infos['date'])
        results['Cell'].append(infos['cellule'])
        results['Condition'].append(condition)
        results['Constant (pN/nm)'].append(k)
        results['Force contact (pN)'].append(parameters['fcontact']*10**12)
        results['Time break (s)'].append(parameters['tstop'])
        results['Distance (µm)'].append(parameters['lretour'])
        results['Speed (µm/s)'].append(vitesse)
        results['Slope (pN/nm)'].append(Young)
        results['[error]'].append(errorYoung)
        results['Sens'].append(direction)

        if fitted == 'fit failed':
            results['Trace\'s fit convergence'].append(False)
        else:
            results['Trace\'s fit convergence'].append(True)

        return({'filename': fichier, 'piconewtons': piconewtons, 'xcorrige': xcorrige, 'fitted': fitted,
                'xnoncorrige': xnoncorrige, 'morceau': morceau, 'model': model})


def courbe_retour(fichier, inputpath, model, factor, eta, jump_force,
                  jump_position, slope_before_jump, alignment_epsilon, condition, results,
                  list_press, pulling_ratio, fitting_frame):
    """Return a dictionnary with all the needed variables to draw the pull curves with matplotlib and perform the classification by filling the results dictionnary.
    Takes as arguments:
    - fichier = the name of the processed file
    - results = dictionnary from which will be written the results.txt output file
    - list press = a list of dictionnaries returned by the courbe_aller() function

    The rest are the parameters entered by the user in the Starting window of the GUI
    - inputpath = the path of the data folder
    - model = the name of the used model
    - factor = the factor to overcome noise
    - eta = the compressibilty constant
    - jump_force
    - jump_position
    - slope_before_jump
    - alignment_epsilon
    - condition
    - pulling_ratio (%)
    - fitting_frame (pts)
    """

    # Fitting functions:

    def FitLin(x, *p):
        fit = np.ones(len(x))
        j = 0
        for i in x:
            fit[j] = p[0]+p[1]*i
            j = j+1
        return fit

    def Get_trace_first_points(fichier):
        who = inputpath+'/'+fichier

        decoupage = Tronconner(who)

        segments = len(decoupage)

        if segments > 1:  # au cas ou la courbe ait un probleme ie arretee a l'aller

            morceau = 'trace'

            # recupere les noms des colonnes au cas ou l'export ait ete different de toutes les infos
            chaine = decoupage[QuelSegment(segments, morceau)]

            lignes = chaine.splitlines()

            # n'est-ce pas la même chose que la variable $lines ?
            aller = decoupage[0].splitlines()

            parameters = RecupereDataFromHeaderAller(aller, segments)

            direction = RecoverDirectionFromAngle(
                parameters['scanner'], parameters['phi'])

            # transforme la chaine en csv
            interim = StringIO(chaine)

            # recupere la liste des noms de colonnes
            listenoms = parameters['noms']

            # cree le dataframe correspondant au morceau voulu
            subdf = pd.read_csv(interim, delimiter=r"\s+",
                                comment='#', names=listenoms)

            microns = subdf['Displacement']*10**6

            # retournement "à l'afm" et choix du y et de la constante
            if direction == '+X':
                y = subdf['X Signal 1']*10**12  # pN
                k = parameters['kx']  # pN/nm
            elif direction == '-X':
                y = Retourner(subdf['X Signal 1']*10**12)  # pN
                k = parameters['kx']  # pN/nm
            elif direction == '+Y':
                y = subdf['Y Signal 1']*10**12  # pN
                k = parameters['ky']  # pN/nm
            elif direction == '-Y':
                y = Retourner(subdf['Y Signal 1']*10**12)  # pN
                k = parameters['ky']  # pN/nm

            # pour ramener la force a zero pour l'aller avec juste une moyenne sur 1000 pts
            premierspoints = y[0:100]

        return(premierspoints)

    # FIRST CHECK - For each file, firstly we check if the courbe_aller function not returned a 'NaN'. If the file was already classified as 'IN' or 'AL' event, the courbe_retour() function stops here and return a 'NaN'.
    try:
        if list_press[-1] == 'NaN':
            return('NaN')
    except IndexError:
        print('Le premier fichier correspond à une courbe non rejetée')

    morceau = 'retrace'

    who = inputpath+'/'+fichier

    decoupage = Tronconner(who)

    # determiner quel segment on veut

    segments = len(decoupage)

    print("=======================================================")
    print("Source : ", fichier)
    print("----------------------------")
    print("Condition = ", condition)

    print("----------------------------")
    print("Nombre de segments = ", segments)
    print("----------------------------")

    if segments > 1:  # au cas ou la courbe ait un probleme ie arretee a l'aller

        print("OK for processing")
        print("----------------------------")

    infos = GetInfoFileName(fichier)
    chaine = decoupage[QuelSegment(segments, morceau)]
    lignes = chaine.splitlines()

    # n'est-ce pas la même chose que la variable $lines ?
    aller = decoupage[0].splitlines()
    #print("aller: ", aller[1:200])

    parameters = RecupereDataFromHeaderAller(aller, segments)
    #print("paramaters: ", parameters)

    # transforme la chaine en csv
    interim = StringIO(chaine)

    # recupere la liste des noms de colonnes
    listenoms = parameters['noms']

    # cree le dataframe correspondant au morceau voulu
    subdf = pd.read_csv(interim, delimiter=r"\s+",
                        comment='#', names=listenoms)

    microns = subdf['Displacement']*10**6

    # attention devra etre sur le retour...
    pullinglength = (subdf['Displacement'].max() -
                     subdf['Displacement'].min())*10**6
    roundpullinglength = round(pullinglength, 1)
    #print("Distance max de traction: ", pullinglength, "µm")
    #print("Arrondi à: ", roundpullinglength, "µm")

    # CLASSIFICATION - if the condition is true -> file is classified as 'IN' event and the courbe_retour return 'NaN'.
    if incomplete_curves2(pull_distance=roundpullinglength, header_distance=parameters['lretour'], ratio=pulling_ratio):

        results['SD baseline retrace (pN)'].append('NaN')
        results['Min Force (pN)'].append('NaN')
        results['Position Min Force (nm)'].append('NaN')
        results['Jump force (pN)'].append('NaN')
        results['Jump end (nm)'].append('NaN')
        results['Pente (pN/nm)'].append('NaN')
        results['Tube cassé ?'].append('NaN')
        results['Aire au min (pN*nm)'].append('NaN')
        results['Aire au jump (pN*nm)'].append('NaN')
        results['Type of event'].append('IN')
        results['Type of event (corrected)'].append('IN')
        results['Retrace Fitting frame (#pts)'].append('NaN')
        results['Retrace\'s fit convergence'].append('NaN')

        print("INCOMPLET")
        return('NaN')

    direction = RecoverDirectionFromAngle(
        parameters['scanner'], parameters['phi'])

    # retournement "à l'afm" et choix du y et de la constante
    if direction == '+X':
        y = subdf['X Signal 1']*10**12  # pN
        k = parameters['kx']  # pN/nm
    elif direction == '-X':
        y = Retourner(subdf['X Signal 1']*10**12)  # pN
        k = parameters['kx']  # pN/nm
    elif direction == '+Y':
        y = subdf['Y Signal 1']*10**12  # pN
        k = parameters['ky']  # pN/nm
    elif direction == '-Y':
        y = Retourner(subdf['Y Signal 1']*10**12)  # pN
        k = parameters['ky']  # pN/nm

    # pour ramener la force a zero pour l'aller avec juste une moyenne sur 1000 pts
    premierspoints = Get_trace_first_points(fichier)
    y0 = premierspoints.mean()
    # print("baseline: ", y0) # moyenne des 1000 premiers points

    # pour ramener la force a zero pour l'aller avec juste une moyenne sur 1000 pts
    dernier = len(y)-1
    # peut être écrit aussi comme: y[-1000:-1]
    dernierspoints = y[dernier-100:dernier]
    # y0=dernierspoints.mean() # si on reprenait la ligne de base sur le retour
    # print "baseline", y0

    piconewtons = y-y0  # ici la correction de ligne de base est faite par l'aller

    deviationstd = piconewtons[dernier-100:dernier].std()
    #print("déviation standard: ", deviationstd)

    TSS = 1000*microns - piconewtons / k  # k est en pN/nm
    xcorrige = np.abs(TSS-TSS[0])

    # point de contact retour via valeur absolue
    minimum = piconewtons.min()
    # renvoi index(numéro de ligne) de la valeur la + faible
    position = piconewtons.idxmin()
    # print "Force min = ", minimum
    # print "Index Force min =", position
    #print("point_de_contact/force_minimum: ", minimum)
    #print("position_force_minimum: ", position)

    # ---------------------------------------------------
    minimumposition = xcorrige[position]
    #plt.scatter(minimumposition, minimum, color='red', label='minimum')
    sousset = piconewtons[0:position]

    vshape = np.abs(sousset)
    minimumvshape = vshape.min()
    vshapeposition = vshape.idxmin()
    minimumvshapeposition = xcorrige[vshapeposition]

    # sortie du bruit
    overcoming = factor*deviationstd

    # valeur finale de la courbe pour les tubes non finis
    # ceci permet d'eviter des soucis de spikes
    valeursfincourbe = piconewtons[dernier-50: dernier]
    moyvalfin = valeursfincourbe.mean()
    # print moyvalfin, -overcoming
    #print("overcoming: ", overcoming)
    #print("moyvalfin: ", moyvalfin)
    #print("valeursfincourbe: ", valeursfincourbe)

    # It's a precaution if the user not enters an integer number in the GUI for the fitting_frame parameter
    frame = int(round(fitting_frame))

    # cas des non adherentes ou ne sortant pas du seuil
    if minimum > -overcoming:
        pointfin = 'NaN'
        positionjump = 'NaN'
        jumpheight = 0.0 # la logique voudrait que l'on mette NaN
        # le tube n'est pas casse
        fini = 'NoAdh'
        longueur = 0.0
        aireaumin = 'NaN'
        aireaujump = 'NaN'
        distanceminimum = 'NaN'
        print('Non adhesif / ne sort pas du seuil')
        pente = np.abs(piconewtons.min() - moyvalfin) / np.abs(minimumposition - minimumvshapeposition)
        fitted = 'no fit'
        # xzonefit='NaN'
        #fitted ='NaN'

        if jumpheight < float(jump_force) and longueur < float(jump_position):
            results['Type of event'].append('NAd')
            results['Type of event (corrected)'].append('NAd')

        output_dict = {'filename': fichier, 'morceau': morceau, 'piconewtons': piconewtons, 'xcorrige': xcorrige, 'minimumposition': minimumposition,
                       'minimum': minimum, 'minimumvshapeposition': minimumvshapeposition, 'minimumvshape': minimumvshape,
                       'overcoming': overcoming, 'factor': factor, 'model': model,
                       'positionjump': positionjump, 'jumpheight': jumpheight, 'fini': fini}

    else:

        if moyvalfin < -overcoming:
            pointfin = dernier
            positionjump = xcorrige[dernier]
            jumpheight = piconewtons[dernier]
            # le tube n'est pas casse
            fini = 'no'

            zonefit = piconewtons[dernier-(frame):dernier]
            #print("zone_fit: ", zonefit)
            xzonefit = xcorrige[dernier-(frame):dernier]
            #print("xzonefit: ", xzonefit)

            try:

                fitParams, fitCovariances = curve_fit(
                    FitLin, xzonefit, zonefit, p0=initialguesslin)
                fitted = FitLin(xzonefit, fitParams[0], fitParams[1])

            except TypeError:
                print('Fit failed !')
                fitted = 'fit failed'

            pente = fitParams[1]
            print('pente (pN/nm) = ', pente)

        else:
            # on cherche dernier en dessous d'un seuil
            retournetmp = piconewtons[::-1]
            ou = np.argmax(retournetmp < -overcoming)
            #print("retournetmp: ", retournetmp[1:10], retournetmp[-10:-1])
            #print("ou: ", ou)

            # print ou

            # on prend le point juste apres
            pointfin = ou+1
            #print("pointfin: ", pointfin)

            # ces valeurs de decalage sont potentiellement a adapter
            decalage = 10
            largeur = 10
            # recovery of step height (approximative)
            endzone = ou-decalage
            #print("endzone: ", endzone)
            startzone = ou-(decalage+largeur)
            #print("startzone: ", startzone)
            zone = piconewtons[startzone:endzone]
            #print("zone: ", zone)
            jumpheight = zone.mean()
            #print("jumpheight: ", jumpheight)
            positionjump = xcorrige[ou-(decalage+largeur/2)]

            # le tube est casse
            fini = 'yes'

            # test de la fenetre de fit pour les tubes finis
            if ou-(frame+10) < 0:
                premier_point = piconewtons[0]
                frame = int(round(ou-premier_point-10))

            zonefit = piconewtons[ou-(frame+10):ou-10]
            xzonefit = xcorrige[ou-(frame+10):ou-10]

            try:
                fitParams, fitCovariances = curve_fit(
                    FitLin, xzonefit, zonefit, p0=initialguesslin)
                fitted = FitLin(xzonefit, fitParams[0], fitParams[1])
                pente = fitParams[1]
                print('pente (pN/nm) = ', pente)

            except TypeError:
                print('Fit failed !')
                fitted = 'fit failed'

        # en disant que pointfin et minvshape sont a zero force pour le triangle
        aireaumin = np.abs(
            (minimum/2.)*(minimumposition-minimumvshapeposition))
        aireaujump = np.abs(
            (jumpheight/2.)*(positionjump-minimumvshapeposition))
        longueur = xcorrige[pointfin]-minimumvshapeposition
        distanceminimum = minimumposition-minimumvshapeposition

        print("Aire au min (pN*nm) =", aireaumin)
        print("Aire au jump (pN*nm) =", aireaujump)
        print("Distance saut (nm) =", longueur)
        print("Saut (pN) =", jumpheight)
        print("Tube fini ? ", fini)

        output_dict = {'filename': fichier, 'morceau': morceau, 'piconewtons': piconewtons, 'xcorrige': xcorrige,
                       'minimumposition': minimumposition, 'minimum': minimum, 'minimumvshapeposition': minimumvshapeposition,
                       'minimumvshape': minimumvshape, 'overcoming': overcoming, 'factor': factor,
                       'xzonefit': xzonefit, 'fitted': fitted, 'model': model, 'xcorrige[pointfin]': xcorrige[pointfin],
                       'piconewtons[pointfin]': piconewtons[pointfin], 'positionjump': positionjump, 'jumpheight': jumpheight, 'fini': fini}

    results['SD baseline retrace (pN)'].append(deviationstd)
    results['Min Force (pN)'].append(np.abs(minimum))
    results['Position Min Force (nm)'].append(distanceminimum)
    results['Jump force (pN)'].append(np.abs(jumpheight))
    results['Jump end (nm)'].append(longueur)
    results['Pente (pN/nm)'].append(pente)
    results['Tube cassé ?'].append(fini)
    results['Aire au min (pN*nm)'].append(aireaumin)
    results['Aire au jump (pN*nm)'].append(aireaujump)
    results['Retrace Fitting frame (#pts)'].append(frame)

    if fitted == 'fit failed' or fitted == 'no fit':
        results['Retrace\'s fit convergence'].append(False)
    else:
        results['Retrace\'s fit convergence'].append(True)

    # CLASSIFICATION - return the adhesion type of event for appending it in the results['Type of event'] and results['Type of event (corrected)'] lists
    # LE try ici permet de vérifier si le fichier n'a pas été déja classé, sinon on appelle la fonction adhesives_events() qui va classer le fichier soit 'AD' soit 'TU'
    try:
        idx = results['Filename'].index(fichier)
        if results['Type of event'][idx] and results['Type of event (corrected)'][idx]:
            pass

    except IndexError:
        adhesion_type = adhesives_events(slope=pente, parameter_slope=slope_before_jump)

        results['Type of event'].append(adhesion_type)
        results['Type of event (corrected)'].append(adhesion_type)

    return(output_dict)


def liste_courbes(path):
    """ Take the path of the folder selected and return the list of files
    contained in this folder."""

    listcourbes = []

    for f in glob.glob(os.path.join(path, '*.txt')):
        os.path.normcase(f)

        listcourbes.append(os.path.split(f)[1])

    listcourbes.sort()

    return(listcourbes)

#----------------------Functions for the Classification------------------------#


def incomplete_curves1(nb_segments, header_segments, tstop):
    """CLASSIFICATION function - Compare the number of segments for the jpk.txt file.

    Takes as arguments:
    - nb_segments = number of segments returned by the decoupage() function
    - header_segments = number of segments reported in the header of the jpk.txt file

    Return True if these two numbers are not equal.
    """

    if nb_segments != header_segments:
        if tstop == 0.0:
            if nb_segments == header_segments - 1:
                print('INCOMPLET avant mais plus maintenant !')
                return False
            else:
                return True

        else:
            return True


def incomplete_curves2(pull_distance, header_distance, ratio):
    """CLASSIFICATION function - Compare the pull_distance calculated with a percentage of the pull distance reported in the header of the jpk.txt file.

    Takes as arguments:
    - pull_distance = distance of pulling calculated (point of jump end - last curve point)
    - header_distance = pulling distance retrieved in the header by the GetInfoFileName() function
    - ratio = percentage entered by the user in the GUI

    Returns True if True if pull_distance is inferior or equal to ratio*header_distance."""
    if pull_distance <= (ratio/100)*header_distance:
        return True


def optical_alignment_curves(header_fcontact, moy_premiers_points, direction, dataframe, epsilon):
    """CLASSIFICATION function - Compare the contact force in all directions (X,Y,Z) for one jpk.txt file.

    Takes as arguments:
    - header_fcontact = force contact reported in the header of the jpk.txt file
    - moy_premiers_points = mean of the first curve points
    - direction = direction from which the signal was measured (Signal X, Y, Z)
    - dataframe = dataframe containing the signal values
    - epsilon = its a factor entered by the user in the GUI

    Returns True if the contact force is superior to a part of the contact force in the direction of interest.
    """
    F1 = np.abs(header_fcontact - moy_premiers_points)

    if 'Y' in direction:
        X_premiers_points = dataframe['X Signal 1'][0:100]
        moy_X_premiers_points = X_premiers_points.mean()
        F2 = np.abs(header_fcontact - moy_X_premiers_points)

        Z_premiers_points = dataframe['Z Signal 1'][0:100]
        moy_Z_premiers_points = Z_premiers_points.mean()
        F3 = np.abs(header_fcontact - moy_Z_premiers_points)

    if 'X' in direction:
        Y_premiers_points = dataframe['Y Signal 1'][0:100]
        moy_Y_premiers_points = Y_premiers_points.mean()
        F2 = np.abs(header_fcontact - moy_Y_premiers_points)

        Z_premiers_points = dataframe['Z Signal 1'][0:100]
        moy_Z_premiers_points = Z_premiers_points.mean()
        F3 = np.abs(header_fcontact - moy_Z_premiers_points)

    if F2 > F1/(100/epsilon) or F3 > F1/(100/epsilon):
        return True


def adhesives_events(slope, parameter_slope):
    """CLASSIFICATION function - Compare jump force values and jump end positions.

    Take as arguments:
    - jump_force = value measured
    - jump_end = position calculated from the data
    - parameter_jump_end = parameter/criteria entered by the user in the GUI
    - parameter_jforce = parameter/criteria entered by the user in the GUI
    - slope = calculated in courbe_retour() function
    - parameter_slope = value/parameter entered by the user in the GUI

    For non-adhesive event:
    - Returns 'NAd' if the jump force measured and the jump end position are lower than the parameters (parameter_jforce & parameter_jump_end)

    For Tube events:
    - Returns 'TU' if the slope is between -parameter_slope and parameter_slope

    For Adhesion event:
    - Returns 'AD' if the two last conditions are False
    """

    slope = float(slope)
    parameter_slope = float(parameter_slope)

    if -parameter_slope < slope < parameter_slope:
        return('TU')
    else:
        return('AD')

#------------------------------------------------------------------------------#


def method_file(path, now, condition, model, radius, factor, eta, type_classification,
                epsilon, jump_force, jump_position, slope_before_jump, fitting_frame, pulling_ratio):
    """Creates an output folder and write a method.txt file in it.
    Takes as arguments all the parameters entered by the user in the GUI Starting window.
    And write thems in txt file.

    NB: the argument <now> is the time from when the <Proceed> button was clicked.
    """

    output_path = path + '/output-' + str(now)

    os.makedirs(output_path)

    filename = now + '-method.txt'

    f = open(filename, 'w+')

    f.write('# Path to data\n')
    f.write(path + '\n')
    f.write('# Condition\n')
    f.write(condition + '\n')
    f.write('# Model name for trace\n')
    f.write(model + '\n')
    f.write('# Radius of bead (µm)\n')
    f.write(str(radius) + '\n')
    f.write('# Factor to overcome noise\n')
    f.write(str(factor) + '\n')
    f.write('# ETA (compressibilty : non compressible is 0.5)\n')
    f.write(str(eta) + '\n')
    f.write('# ' + type_classification +
            ' classification - used parameters: \n')
    f.write('# Epsilon (%)\n')
    f.write(str(epsilon) + '\n')
    f.write('# Pulling lenght / setting (%)\n')
    f.write(str(pulling_ratio) + '\n')
    f.write('# Jump force (pN)\n')
    f.write(str(jump_force) + '\n')
    f.write('# Jump position (nm)\n')
    f.write(str(jump_position) + '\n')
    f.write('# Slope before jump (-x < slope < +x)\n')
    f.write(str(slope_before_jump) + '\n')
    f.write('# Fit before jump over (#pts)\n')
    f.write(str(fitting_frame) + '\n')

    f.close()

    os.rename(filename, output_path + '/' + filename)


def Output_results_file(path, now, results):
    """Writes the results.txt file in the output folder.

    Takes as arguments:
    - now = time from when the <Proceed> button was clicked in the GUI
    - path = data folder path
    - results = dictionnary which contains all the informations to write results.txt file
    """

    output_path = path + '/output-' + str(now)

    filename = now + '-results.txt'

    df = pd.DataFrame.from_dict(results, orient='columns', dtype=None)
    # print(df)

    desired_columns_order = ['Filename', 'Type of event', 'Type of event (corrected)', 'Date', 'Hour', 'Condition',
                             'Couple', 'Bead', 'Cell', 'Direction', 'Sens', 'Constant (pN/nm)', 'Force contact (pN)',
                             'Time break (s)', 'Distance (µm)', 'Speed (µm/s)', 'Trace\'s fit convergence', 'Slope (pN/nm)',
                             '[error]', 'SD baseline retrace (pN)', 'Min Force (pN)', 'Position Min Force (nm)',
                             'Jump force (pN)', 'Jump end (nm)', 'Retrace\'s fit convergence', 'Retrace Fitting frame (#pts)',
                             'Pente (pN/nm)', 'Tube cassé ?', 'Aire au min (pN*nm)', 'Aire au jump (pN*nm)']

    df = df[desired_columns_order]

    df.to_csv(filename, sep='\t', encoding='utf-8')

    os.rename(filename, output_path + '/' + filename)


def Draw_scatter_slope(results, noise_factor, slope_before_jump, jump_position):
    """Displays two matplotlib windows of two scatter plots.

    Takes as arguments:
    - results = results dictionnary
    - noise_factor = parameter entred by the user in the GUI
    - slope_before_jump = parameter entred by the user in the GUI
    - jump_position = parameter entred by the user in the GUI

    One scatter plot shows jump force according to slope before jump.
    And the second plot shows jump force according to jump position.
    """

    df = pd.DataFrame.from_dict(results, orient='columns', dtype=None)
    for x in list(df):
        for i in np.arange(len(df.loc[:, x])):
            if df.loc[i, x] == 'NaN':
                df.loc[i, x] = np.nan

    #df['Pente (pN/nm)'].astype(float)
    #df['Jump force (pN)'].astype(float)
    #df['SD baseline retrace (pN)'].astype(float)

    SD = df['SD baseline retrace (pN)'].mean()
    zone = noise_factor*SD

    fig = plt.figure("Jump Force = f(Slope before jump)",
                     figsize=(5, 5), dpi=100)
    plt.scatter(df.loc[:, 'Pente (pN/nm)'], df.loc[:,
                                                   'Jump force (pN)'], label='', color='grey', alpha=0.5)
    plt.axhline(y=zone, alpha=0.5, color='red',
                ls="--", label=str(round(zone, 1))+'pN')
    plt.xlabel('Slope before jump (pN/nm)')
    plt.ylabel('Jump force (pN)')

    for i in np.arange(len(df)):
        if (df.loc[i, 'Type of event (corrected)'] == 'AD') and (df.loc[i, 'Tube cassé ?'] != 'NoAdh'):
            if df.loc[i, 'Jump force (pN)'] > zone:
                plt.scatter(df.loc[i, 'Pente (pN/nm)'], df.loc[i,
                                                               'Jump force (pN)'], color='red', alpha=1, s=10)

        if df.loc[i, 'Type of event (corrected)'] == 'TU' and (df.loc[i, 'Tube cassé ?'] != 'NoAdh'):
            if df.loc[i, 'Jump force (pN)'] > zone:
                plt.scatter(df.loc[i, 'Pente (pN/nm)'], df.loc[i,
                                                               'Jump force (pN)'], color='green', alpha=1, s=10)

    plt.title('Adhesion : red | Tube : green')
    plt.axvline(x=slope_before_jump, alpha=0.5, color='red',
                ls="--", label=str(slope_before_jump) + ' pN/nm')
    plt.axvline(x=-slope_before_jump, alpha=0.5, color='red',
                ls="-", label=str(-slope_before_jump) + ' pN/nm')

    plt.legend()
    # plt.show()

    fig2 = plt.figure("Jump force = f(Jump position)", figsize=(5, 5), dpi=100)
    plt.scatter(df.loc[:, 'Jump end (nm)'], df.loc[:,
                                                   'Jump force (pN)'], label='', color='grey', alpha=0.5)
    plt.axhline(y=zone, alpha=0.5, color='red',
                ls="--", label=str(round(zone, 1))+'pN')
    plt.axvline(x=1000, alpha=0.5, color='black', ls="--", label='1000 nm')
    plt.axvline(x=500, alpha=0.5, color='black', ls="-", label='500 nm')
    plt.axvline(x=jump_position, alpha=0.5, color='red',
                ls='-', label=str(jump_position)+' nm')

    plt.xlabel('Jump position (nm)')
    plt.ylabel('Jump force (pN)')

    for i in np.arange(len(df)):
        if (df.loc[i, 'Type of event (corrected)'] == 'AD') and (df.loc[i, 'Tube cassé ?'] != 'NoAdh'):
            if df.loc[i, 'Jump force (pN)'] > zone:
                plt.scatter(df.loc[i, 'Jump end (nm)'], df.loc[i,
                                                               'Jump force (pN)'], color='red', alpha=1, s=10)
                # LAdh.append(full['finjump'][i])
                # FAdh.append(full['Fjump'][i])
        if df.loc[i, 'Type of event (corrected)'] == 'TU' and (df.loc[i, 'Tube cassé ?'] != 'NoAdh'):
            if df.loc[i, 'Jump force (pN)'] > zone:
                plt.scatter(df.loc[i, 'Jump end (nm)'], df.loc[i,
                                                               'Jump force (pN)'], color='green', alpha=1, s=10)
                # LTube.append(full['finjump'][i])
                # FTube.append(full['Fjump'][i])

    plt.xscale("log")
    plt.title('Adhesion : red | Tube : green')
    plt.legend()
    plt.show()


if __name__ == "__main__":
    start = time.time()
    liste_fichiers = liste_courbes()
    a = preprocessing_aller_test(liste_fichiers)
    b = preprocessing_retour_test(liste_fichiers)
    print('nombres de fichiers traités: ', len(liste_fichiers))
    print('%ds elapsed' % (time.time() - start))
    # Courbe_aller('b10c10b-2017.11.17-16.59.52.512.txt')
    # courbe_retour('b10c10b-2017.11.17-16.59.52.512.txt')
    # print(liste_courbes())
