# -*- coding: utf-8 -*-

#--------modules imported for running the GUI----------------------------------#
import os
import sys
import glob
import time

from tkinter import Tk, messagebox, FLAT, filedialog, TOP, LabelFrame, IntVar, Toplevel, StringVar, Menu, Canvas, Label, Entry, Button, NW, BOTH, Frame, Scrollbar, N, S, VERTICAL, Menu, HORIZONTAL, RIGHT, Y, E, W, Text, END, GROOVE, RIDGE, Radiobutton, LEFT, CENTER, DoubleVar, X

import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.backend_bases import key_press_handler
import matplotlib.patches as mpatches
import numpy as np
from operator import add, sub

from threading import Thread, RLock  # module needed for multi-threading

# model script which contains functions for data processing
sys.path.append('./Model')
import model_final as model_final


#---------------Creation of the parent window (Tk)-----------------------------#

class Start_window(Tk):
    """ Class of the main and starting window where the user enter all
    parameters to launch the classification.
    """

    def __init__(self, parent):
        Tk.__init__(self, parent)

        self.parent = parent  # When we create a widget we keep a reference of our parent widget
        self.initialize()
        self.height = str(self.winfo_screenwidth())
        self.width = str(self.winfo_screenheight())
        # When a window is open, it takes the whole screen size
        self.geometry(self.height + 'x' + self.width)

    def initialize(self):
        """ This function contains all the graphical elements of the starting
        window and the functions for each button."""

        # Initialization of needed variables:

        self.__class__.variable_model = StringVar(value='linear')
        self.__class__.variable_condition = StringVar()
        self.__class__.variable_bead_radius = DoubleVar(value=1.1)
        self.__class__.variable_noise_factor = DoubleVar(value=7.0)
        self.__class__.variable_jump_force = DoubleVar(value=5.0)
        self.__class__.variable_jump_position = DoubleVar(value=200)
        self.__class__.variable_slope_before_jump = DoubleVar(value=0.025)
        self.__class__.variable_alignment_epsilon = DoubleVar(value=25)
        self.__class__.variable_type_classification = StringVar()
        self.__class__.variable_eta = DoubleVar(value=0.5)
        self.__class__.variable_fitting_frame = IntVar(value=200)
        self.__class__.variable_pulling_ratio = DoubleVar(value=50.0)

        def Get_data_folder_path():
            """ Function related to the <Choose data folder> button.
            Open a filedialog box and initialize the <folder_path> global variable
            """

            global folder_path
            folder_path = filedialog.askdirectory(
                initialdir=os.path.dirname('../'), mustexist=True)

            # print(folder_path)

        def Load_method_file():
            """Take the parameters from a method file and fill them in the entries.
            - Open a filedialog box to choose the method file and initialize a
             <method_path> global variable.
            - Call the Get_method_parameters from the model and update the
            variables of the entries."""

            # The <method_path> variable is set because it is used also in the <Supervised_window> class
            global method_path
            method_path = filedialog.askopenfilename(title="Open the method file", initialdir=os.path.dirname('../'),
                                                     filetypes=[("txt", "*.txt")])

            self.parameters = model_final.Get_method_parameters(
                outputpath=None, now=None, load_path=method_path)

            self.variable_model.set(self.parameters['model'])
            self.variable_condition.set(self.parameters['condition'])
            self.variable_bead_radius.set(self.parameters['bead_radius'])
            self.variable_noise_factor.set(self.parameters['noise_factor'])
            self.variable_jump_force.set(self.parameters['jump_force'])
            self.variable_jump_position.set(self.parameters['jump_position'])
            self.variable_slope_before_jump.set(
                self.parameters['slope_before_jump'])
            self.variable_alignment_epsilon.set(self.parameters['epsilon'])
            self.variable_eta.set(self.parameters['eta'])
            self.variable_fitting_frame.set(self.parameters['fitting_frame'])
            self.variable_pulling_ratio.set(self.parameters['pulling_ratio'])

        def Display_press_pull_curves(index, list_press, list_pull):
            """ Draw the press and pull curves displayed 3 by 3 and saves them into .png file in the working directory.

            - the index argument is a list of 3 indexes where each corresponds
            to a curve.txt in the list_press & list_pull.
            - list_press is a list which contains all the names of all jpk.txt to draw the press curves.
            - list_pull contains all the names of all jpk.txt curve to draw the pull curves.

            This function use the dictionnaries returned by the Courbe_aller()
            and courbe_retour() functions from model_final.py script.
            These dictionnaries contains all variables necessary to draw the matplotlib plots.
            """

            # Retrieve the indexes
            x1 = index[0]
            x2 = index[1]
            x3 = index[2]

            # Retrieve the needed dictionnaries of variables to draw the curves
            aller1 = list_press[x1]
            aller2 = list_press[x2]
            aller3 = list_press[x3]
            retour1 = list_pull[x1]
            retour2 = list_pull[x2]
            retour3 = list_pull[x3]

            # Initialization of the matplotlib figure
            fig1 = Figure(dpi=80)
            fig1.set_size_inches(10, 7.5, forward=True)
            fig1.text(0.5, 0.65, aller1['filename'],
                      horizontalalignment='center', style='italic')
            fig1.text(0.5, 0.35, aller2['filename'],
                      horizontalalignment='center', style='italic')
            fig1.text(0.5, 0.05, aller3['filename'],
                      horizontalalignment='center', style='italic')
            a1 = fig1.add_subplot(321)
            a2 = fig1.add_subplot(323)
            a3 = fig1.add_subplot(325)
            b1 = fig1.add_subplot(322)
            b2 = fig1.add_subplot(324)
            b3 = fig1.add_subplot(326)

            #-----------------------press curves initialization-----------------#

            a1.plot(aller1['xcorrige'], aller1['piconewtons'],
                    color='blue', alpha=0.5, label='_nolegend_')
            a1.set_xlabel('TSS (nm)')
            a1.set_ylabel('F (pN)')

            # Test if the fit converge or not
            if aller1['fitted'] == 'fit failed':
                handles, labels = a1.get_legend_handles_labels()
                # Add in the legend the label 'Fit Failed'
                handles.append(mpatches.Patch(color='red', label='Fit failed'))
                a1.legend(handles=handles, bbox_to_anchor=(0.5, 1.2),
                          loc='upper center', ncol=3, fancybox=True)

            else:
                a1.plot(aller1['xcorrige'], aller1['fitted'],
                        color='black', alpha=0.75, label='fit' + aller1['model'])
                a1.legend(bbox_to_anchor=(0.5, 1.2),
                          loc='upper center', ncol=3, fancybox=True)

            a2.plot(aller2['xcorrige'], aller2['piconewtons'],
                    color='blue', alpha=0.5, label='_nolegend_')
            a2.set_xlabel('TSS (nm)')
            a2.set_ylabel('F (pN)')

            if aller2['fitted'] == 'fit failed':
                handles, labels = a2.get_legend_handles_labels()
                handles.append(mpatches.Patch(color='red', label='Fit failed'))
                a2.legend(handles=handles)

            else:
                a2.plot(aller2['xcorrige'], aller2['fitted'],
                        color='black', alpha=0.75, label='fit' + aller2['model'])

            a3.plot(aller3['xcorrige'], aller3['piconewtons'],
                    color='blue', alpha=0.5, label='_nolegend_')
            a3.set_xlabel('TSS (nm)')
            a3.set_ylabel('F (pN)')

            if aller3['fitted'] == 'fit failed':
                handles, labels = a3.get_legend_handles_labels()
                handles.append(mpatches.Patch(color='red', label='Fit failed'))
                a3.legend(handles=handles)

            else:
                a3.plot(aller3['xcorrige'], aller3['fitted'],
                        color='black', alpha=0.75, label='fit' + aller3['model'])
            #------------------------------------------------------------------#

            #----------------------Pull curves initialization------------------#

            # Test if we have a non-Adhesive event
            if retour1['fini'] == 'NoAdh':
                b1.plot(retour1['xcorrige'], retour1['piconewtons'],
                        color='green', alpha=0.5, label='_nolegend_')
                b1.set_xlabel('TSS (nm)')
                b1.set_ylabel('F (pN)')

                b1.scatter(retour1['minimumposition'],
                           retour1['minimum'], color='red', label='minimum')
                b1.scatter(retour1['minimumvshapeposition'],
                           retour1['minimumvshape'], color='black', label='zero')
                b1.axhline(y=-(retour1['overcoming']), alpha=0.5, ls='-.', label=str(
                    retour1['factor']) + ' x SD = '+str(round(retour1['overcoming'], 1))+'pN')
                b1.axhline(y=0, alpha=0.25, ls='-.')
                b1.legend(bbox_to_anchor=(0.5, 1.3),
                          loc='upper center', ncol=3, fancybox=True)

            else:
                b1.plot(retour1['xcorrige'], retour1['piconewtons'],
                        color='green', alpha=0.5, label='_nolegend_')
                b1.set_xlabel('TSS (nm)')
                b1.set_ylabel('F (pN)')

                b1.scatter(retour1['minimumposition'],
                           retour1['minimum'], color='red', label='minimum')
                b1.scatter(retour1['minimumvshapeposition'],
                           retour1['minimumvshape'], color='black', label='zero')
                b1.axhline(y=-(retour1['overcoming']), alpha=0.5, ls='-.', label=str(
                    retour1['factor']) + ' x SD = '+str(round(retour1['overcoming'], 1))+'pN')
                b1.axhline(y=0, alpha=0.25, ls='-.')

                b1.scatter(retour1['xcorrige[pointfin]'], retour1['piconewtons[pointfin]'],
                           color='green', alpha=1, label='fin saut')
                b1.scatter(retour1['positionjump'], retour1['jumpheight'],
                           color='blue', alpha=1, label='jump')

                # Test if the fit converged or not
                if retour1['fitted'] == 'fit failed':
                    handles, labels = b1.get_legend_handles_labels()
                    handles.append(mpatches.Patch(
                        color='red', label='Fit failed'))
                    b1.legend(handles=handles, bbox_to_anchor=(
                        0.5, 1.3), loc='upper center', ncol=3, fancybox=True)
                else:
                    b1.plot(retour1['xzonefit'], retour1['fitted'],
                            color='black', alpha=0.75, label='fit' + retour1['model'])
                    b1.legend(bbox_to_anchor=(0.5, 1.3),
                              loc='upper center', ncol=3, fancybox=True)

            if retour2['fini'] == 'NoAdh':
                b2.plot(retour2['xcorrige'], retour2['piconewtons'],
                        color='green', alpha=0.5, label='_nolegend_')
                b2.set_xlabel('TSS (nm)')
                b2.set_ylabel('F (pN)')

                b2.scatter(
                    retour2['minimumposition'], retour2['minimum'], color='red', label='_nolegend_')
                b2.scatter(retour2['minimumvshapeposition'],
                           retour2['minimumvshape'], color='black', label='_nolegend_')
                b2.axhline(y=-(retour2['overcoming']), alpha=0.5, ls='-.', label=str(
                    retour2['factor']) + ' x SD = '+str(round(retour2['overcoming'], 1))+'pN')
                b2.axhline(y=0, alpha=0.25, ls='-.')
                b2.legend()

            else:
                b2.plot(retour2['xcorrige'], retour2['piconewtons'],
                        color='green', alpha=0.5, label='_nolegend_')
                b2.set_xlabel('TSS (nm)')
                b2.set_ylabel('F (pN)')

                b2.scatter(
                    retour2['minimumposition'], retour2['minimum'], color='red', label='_nolegend_')
                b2.scatter(retour2['minimumvshapeposition'],
                           retour2['minimumvshape'], color='black', label='_nolegend_')
                b2.axhline(y=-(retour2['overcoming']), alpha=0.5, ls='-.', label=str(
                    retour2['factor']) + ' x SD = '+str(round(retour2['overcoming'], 1))+'pN')
                b2.axhline(y=0, alpha=0.25, ls='-.')

                b2.scatter(retour2['xcorrige[pointfin]'], retour2['piconewtons[pointfin]'],
                           color='green', alpha=1, label='_nolegend_')
                b2.scatter(retour2['positionjump'], retour2['jumpheight'],
                           color='blue', alpha=1, label='_nolegend_')

                if retour2['fitted'] == 'fit failed':
                    handles, labels = b2.get_legend_handles_labels()
                    handles.append(mpatches.Patch(
                        color='red', label='Fit failed'))
                    b2.legend(handles=handles)

                else:
                    b2.plot(retour2['xzonefit'], retour2['fitted'],
                            color='black', alpha=0.75, label='_nolegend_')
                    b2.legend()

            if retour3['fini'] == 'NoAdh':
                b3.plot(retour3['xcorrige'], retour3['piconewtons'],
                        color='green', alpha=0.5, label='_nolegend_')
                b3.set_xlabel('TSS (nm)')
                b3.set_ylabel('F (pN)')

                b3.scatter(
                    retour3['minimumposition'], retour3['minimum'], color='red', label='_nolegend_')
                b3.scatter(retour3['minimumvshapeposition'],
                           retour3['minimumvshape'], color='black', label='_nolegend_')
                b3.axhline(y=-(retour3['overcoming']), alpha=0.5, ls='-.', label=str(
                    retour3['factor']) + ' x SD = '+str(round(retour3['overcoming'], 1))+'pN')
                b3.axhline(y=0, alpha=0.25, ls='-.')
                b3.legend()

            else:
                b3.plot(retour3['xcorrige'], retour3['piconewtons'],
                        color='green', alpha=0.5, label='_nolegend_')
                b3.set_xlabel('TSS (nm)')
                b3.set_ylabel('F (pN)')

                b3.scatter(
                    retour3['minimumposition'], retour3['minimum'], color='red', label='_nolegend_')
                b3.scatter(retour3['minimumvshapeposition'],
                           retour3['minimumvshape'], color='black', label='_nolegend_')
                b3.axhline(y=-(retour3['overcoming']), alpha=0.5, ls='-.', label=str(
                    retour3['factor']) + ' x SD = '+str(round(retour3['overcoming'], 1))+'pN')
                b3.axhline(y=0, alpha=0.25, ls='-.')

                b3.scatter(retour3['xcorrige[pointfin]'], retour3['piconewtons[pointfin]'],
                           color='green', alpha=1, label='_nolegend_')
                b3.scatter(retour3['positionjump'], retour3['jumpheight'],
                           color='blue', alpha=1, label='_nolegend_')

                if retour3['fitted'] == 'fit failed':
                    handles, labels = b3.get_legend_handles_labels()
                    handles.append(mpatches.Patch(
                        color='red', label='Fit failed'))
                    b3.legend(handles=handles)

                else:
                    b3.plot(retour3['xzonefit'], retour3['fitted'],
                            color='black', alpha=0.75, label='_nolegend_')
                    b3.legend()

            # Adjust the space between the curves and the edges of the figure
            fig1.tight_layout(pad=2.5, h_pad=1)

            plotname = str(x1)+'-'+str(x2)+'-'+str(x3)+'.png'

            # Save the matplotlib figure in the working directory
            fig1.savefig(plotname)
            plt.close(fig1)  # For release memory

        def Unsupervised_data_processing():
            """Function which performs the unsupervised classifications.
            It communicates by self.Label_processing and self.variable_progress
            with the <unsupervised_window> Toplevel to display the progress
            during the data processing.

            - 1) Write the method file in a output path
            - 2) Initialization of the variables:
            self.results (dict for writing the results.txt file), self.list_filenames,
            list_pull and list_press
            - 3) Filling of the lists and self.results dictionnary
            - 4) Write the results.txt file in the output path
            - 5) Draw the curves (3 by 3) and save them in the output path"""

            #---------------Initialization of the variables--------------------#

            output_path = folder_path + '/output-' + str(now)

            model_final.method_file(path=folder_path, now=now,
                                    condition=self.variable_condition.get(), model=self.variable_model.get(),
                                    radius=self.variable_bead_radius.get(), factor=self.variable_noise_factor.get(),
                                    eta=self.variable_eta.get(), type_classification=self.variable_type_classification.get(),
                                    epsilon=self.variable_alignment_epsilon.get(), jump_force=self.variable_jump_force.get(),
                                    jump_position=self.variable_jump_position.get(), slope_before_jump=self.variable_slope_before_jump.get(),
                                    pulling_ratio=self.variable_pulling_ratio.get(), fitting_frame=self.variable_fitting_frame.get())

            self.__class__.results = {'Filename': [], 'Date': [], 'Hour': [], 'Condition': [], 'Couple': [], 'Bead': [],
                                      'Cell': [], 'Direction': [], 'Sens': [], 'Constant (pN/nm)': [],
                                      'Force contact (pN)': [], 'Time break (s)': [], 'Distance (µm)': [],
                                      'Speed (µm/s)': [], 'Slope (pN/nm)': [], '[error]': [],
                                      'SD baseline retrace (pN)': [], 'Min Force (pN)': [], 'Position Min Force (nm)': [],
                                      'Jump force (pN)': [], 'Jump end (nm)': [], 'Pente (pN/nm)': [], 'Tube cassé ?': [],
                                      'Aire au min (pN*nm)': [], 'Aire au jump (pN*nm)': [], 'Type of event': [],
                                      'Trace\'s fit convergence': [], 'Retrace\'s fit convergence': [], 'Retrace Fitting frame (#pts)': [],
                                      'Type of event (corrected)': []}

            self.__class__.list_filenames = model_final.liste_courbes(
                folder_path)
            list_press = []
            list_pull = []

            #------------------------------------------------------------------#

            for x in self.list_filenames:
                if self.variable_stop.get() == 'stop':
                    break
                else:
                    self.variable_progress.set(str(self.list_filenames.index(
                        x)+1) + ' / ' + str(len(self.list_filenames)))

                    list_press.append(model_final.Courbe_aller(fichier=x, inputpath=folder_path,
                                                               model=self.variable_model.get(), factor=self.variable_noise_factor.get(),
                                                               eta=self.variable_eta.get(), jump_force=self.variable_jump_force.get(),
                                                               jump_position=self.variable_jump_position.get(),
                                                               slope_before_jump=self.variable_slope_before_jump.get(),
                                                               alignment_epsilon=self.variable_alignment_epsilon.get(),
                                                               condition=self.variable_condition.get(), results=self.results,
                                                               list_press= list_press, list_pull= list_pull))
                    list_pull.append(model_final.courbe_retour(fichier=x, inputpath=folder_path,
                                                               model=self.variable_model.get(), factor=self.variable_noise_factor.get(),
                                                               eta=self.variable_eta.get(), jump_force=self.variable_jump_force.get(),
                                                               jump_position=self.variable_jump_position.get(),
                                                               slope_before_jump=self.variable_slope_before_jump.get(),
                                                               alignment_epsilon=self.variable_alignment_epsilon.get(),
                                                               condition=self.variable_condition.get(), results=self.results,
                                                               list_press=list_press, pulling_ratio=self.variable_pulling_ratio.get(),
                                                               fitting_frame=self.variable_fitting_frame.get()))

                    # Verification dans le cas ou le fichier est classé incomplet dans la phase retour
                    if list_pull[-1] == 'NaN':
                        list_press.pop(-1)
                        list_pull.pop(-1)

                    self.file_number = self.list_filenames.index(x)

            print(len(list_press))
            print(len(list_pull))
            for x in self.results.keys():
                print(len(self.results[x]), x)

            model_final.Output_results_file(
                path=folder_path, now=now, results=self.results)

            t, s = divmod(len(list_pull), 3)
            self.Label_processing.configure(text='Saving curves...')

            for i in range(0, len(list_pull), 3):
                index = [i, i+1, i+2]
                self.variable_progress.set(
                    str(i) + ' / ' + str(len(list_pull)-1))
                if i == t*3:
                    index = [len(list_pull) - 3, len(list_pull) -
                             2, len(list_pull) - 1]
                    Display_press_pull_curves(index, list_press, list_pull)

                else:
                    Display_press_pull_curves(index, list_press, list_pull)

            self.Label_processing.configure(text='Processing finished')
            counter = {'IN': self.results['Type of event'].count('IN'), 'AL': self.results['Type of event'].count('AL'),
                       'NAd': self.results['Type of event'].count('NAd'), 'AD': self.results['Type of event'].count('AD'), 'TU': self.results['Type of event'].count('TU')}
            self.variable_progress.set('\n' + 'Total of processed files: ' + str(self.file_number+1) + ' / ' + str(len(self.list_filenames)) + '\n\n' + 'IN: ' + str(counter['IN']) + '  AL: ' + str(counter['AL']) +
                                       '  NAd: ' + str(counter['NAd']) + '  AD: ' + str(counter['AD']) + '  TU: ' + str(counter['TU']) + '\n')

            current_directory = os.getcwd()
            for f in glob.glob(os.path.join(current_directory, '*.png')):
                filename = os.path.split(f)[1]
                os.rename(f, output_path + '/' + filename)

        class Unsupervised_processing(Thread):
            """This thread run the Unsupervised_data_processing function.
            For running it at the same time as we display the
            <unsupervised_window> toplevel to avoid the freeze of the GUI."""

            def __init__(self):
                Thread.__init__(self)

            def run(self):
                Unsupervised_data_processing()

        def Stop_processing():
            """Function for the <STOP> button, it sets the value of
            self.variable_stop to 'stop' which turns off the Unsupervised_data_processing."""

            self.variable_stop.set('stop')

        def Get_unsupervised_window():
            """Creates a Toplevel with <STOP> and <Draw scatter plots> buttons,
            which shows the data processing progression."""

            self.variable_progress = StringVar()
            self.__class__.variable_stop = StringVar()
            self.unsupervised_window = Toplevel(self)
            self.Label_processing = Label(
                self.unsupervised_window, text='Processing Data...')
            self.Label_processing.pack(side=TOP)
            self.Label_progress = Label(
                self.unsupervised_window, textvariable=self.variable_progress)
            self.Label_progress.pack(side=TOP)
            self.Button_stop = Button(self.unsupervised_window, text='STOP',
                                      activebackground="#d9082b", command=lambda: Stop_processing())
            self.Button_stop.pack(side=LEFT)
            self.Button_display_scatter = Button(self.unsupervised_window, text='Draw scatter plot', command=lambda: model_final.Draw_scatter_slope(
                results=self.results, noise_factor=self.variable_noise_factor.get(), slope_before_jump=self.variable_slope_before_jump.get(), jump_position=self.variable_jump_position.get()))
            self.Button_display_scatter.pack(side=TOP)

            Unsupervised_processing().start()

        def Get_supervised_window():
            """Write the method file in the output path and
            Open the supervised window (call the <Supervised_window> class). """

            model_final.method_file(path=folder_path, now=now,
                                    condition=self.variable_condition.get(), model=self.variable_model.get(),
                                    radius=self.variable_bead_radius.get(), factor=self.variable_noise_factor.get(),
                                    eta=self.variable_eta.get(), type_classification=self.variable_type_classification.get(),
                                    epsilon=self.variable_alignment_epsilon.get(), jump_force=self.variable_jump_force.get(),
                                    jump_position=self.variable_jump_position.get(), slope_before_jump=self.variable_slope_before_jump.get(),
                                    pulling_ratio=self.variable_pulling_ratio.get(), fitting_frame=self.variable_fitting_frame.get())

            self.supervised_window = Supervised_window()

            self.supervised_window.lift(self)

        def Proceed_to_classification():
            """Check the type of classification choosen by the user and launches the classification."""

            global now
            now = time.strftime("%Y")+time.strftime("%m")+time.strftime("%d") + \
                '-' + time.strftime("%H")+time.strftime("%M") + \
                time.strftime("%S")

            if self.variable_type_classification.get() == 'unsupervised':
                Get_unsupervised_window()

            if self.variable_type_classification.get() == 'supervised':
                Get_supervised_window()

        #-----Creation of the graphical elements for the Starting Window-------#

        # Creation of the Frames

        self.Frame_method = Frame(
            self, relief=GROOVE, borderwidth='4', width=365)
        self.Frame_method.place(relx=0.1, rely=0.25,
                                relheight=0.42, relwidth=0.28)

        self.Frame_classification = Frame(
            self, relief=GROOVE, borderwidth='4', width=655)
        self.Frame_classification.place(
            relx=0.45, rely=0.08, relheight=0.7, relwidth=0.47)

        # Creations of the method form

        self.Label_method_parameters = Label(
            self.Frame_method, text='Method parameters', font=('Helvetica', 12, 'bold'))
        self.Label_method_parameters.place(
            relx=0.29, rely=0.03, height=19, width=165)

        self.Button_choose_folder = Button(
            self.Frame_method, text='Choose data folder', command=Get_data_folder_path)
        self.Button_choose_folder.place(
            relx=0.1, rely=0.14, height=37, width=130)

        self.Button_load_method = Button(
            self.Frame_method, text='Load method file', command=lambda: Load_method_file())
        self.Button_load_method.place(
            relx=0.57, rely=0.14, height=37, width=117)

        self.Label_model = Label(self.Frame_method, text='model')
        self.Label_model.place(relx=0.16, rely=0.37, height=19, width=64)

        self.Radiobutton_linear = Radiobutton(
            self.Frame_method, text='linear', justify=LEFT, variable=self.variable_model, value='linear')
        self.Radiobutton_linear.place(
            relx=0.44, rely=0.37, relheight=0.06, relwidth=0.16)

        self.Radiobutton_sphere = Radiobutton(
            self.Frame_method, text='sphere', justify=LEFT, variable=self.variable_model, value='sphere')
        self.Radiobutton_sphere.place(
            relx=0.68, rely=0.37, relheight=0.06, relwidth=0.18)

        self.Label_eta = Label(self.Frame_method, text='eta')
        self.Label_eta.place(relx=0.25, rely=0.48, height=19, width=21)

        self.Entry_eta = Entry(
            self.Frame_method, textvariable=self.variable_eta, justify='center')
        self.Entry_eta.place(relx=0.49, rely=0.48, height=19, relwidth=0.4)

        self.Label_condition = Label(self.Frame_method, text='condition')
        self.Label_condition.place(relx=0.19, rely=0.59, height=19, width=54)
        self.Entry_condition = Entry(
            self.Frame_method, textvariable=self.variable_condition, justify='center')
        self.Entry_condition.place(
            relx=0.49, rely=0.59, height=19, relwidth=0.4)
        self.Label_bead_radius = Label(
            self.Frame_method, text='bead radius (µm)')
        self.Label_bead_radius.place(relx=0.14, rely=0.7, height=19, width=97)
        self.Entry_bead_radius = Entry(
            self.Frame_method, textvariable=self.variable_bead_radius, justify='center')
        self.Entry_bead_radius.place(
            relx=0.49, rely=0.7, height=19, relwidth=0.4)

        self.Label_noise_factor = Label(
            self.Frame_method, text='factor to overcome noise')
        self.Label_noise_factor.place(
            relx=0.03, rely=0.82, height=19, width=160)
        self.Entry_noise_factor = Entry(
            self.Frame_method, textvariable=self.variable_noise_factor, justify='center')
        self.Entry_noise_factor.place(
            relx=0.49, rely=0.82, height=19, relwidth=0.4)

        # Creation of the classification form

        self.Label_classification = Label(
            self.Frame_classification, text='Classification parameters', font=('Helvetica', 12, 'bold'))
        self.Label_classification.place(
            relx=0.37, rely=0.03, height=23, width=197)

        self.Labelframe_type_classification = LabelFrame(
            self.Frame_classification, relief=RIDGE, text='Type of classification', width=430)
        self.Labelframe_type_classification.place(
            relx=0.17, rely=0.1, relheight=0.13, relwidth=0.66)

        self.Radiobutton_unsupervised = Radiobutton(
            self.Labelframe_type_classification, text='unsupervised', justify=LEFT, variable=self.variable_type_classification, value='unsupervised')
        self.Radiobutton_unsupervised.place(
            relx=0.21, rely=0.4, relheight=0.29, relwidth=0.25)

        self.Radiobutton_supervised = Radiobutton(
            self.Labelframe_type_classification, text='supervised', justify=LEFT, variable=self.variable_type_classification, value='supervised')
        self.Radiobutton_supervised.place(
            relx=0.6, rely=0.4, relheight=0.29, relwidth=0.21)

        self.Labelframe_rejected_curves = LabelFrame(
            self.Frame_classification, relief=GROOVE, text='Rejected curves', width=430)
        self.Labelframe_rejected_curves.place(
            relx=0.17, rely=0.27, relheight=0.19, relwidth=0.66)

        self.Label_alignment_epsilon = Label(
            self.Labelframe_rejected_curves, text='epsilon (%)')
        self.Label_alignment_epsilon.place(
            relx=0.19, rely=0.26, height=19, width=66)

        self.Entry_alignment_epsilon = Entry(
            self.Labelframe_rejected_curves, textvariable=self.variable_alignment_epsilon, justify='center')
        self.Entry_alignment_epsilon.place(
            relx=0.4, rely=0.26, height=19, relwidth=0.34)

        self.Label_pulling = Label(
            self.Labelframe_rejected_curves, text='Pulling length / setting (%)')
        self.Label_pulling.place(relx=0.07, rely=0.61, height=19, width=150)

        self.Entry_pulling_length = Entry(
            self.Labelframe_rejected_curves, textvariable=self.variable_pulling_ratio, justify='center')
        self.Entry_pulling_length.place(
            relx=0.47, rely=0.61, height=19, relwidth=0.34)

        self.Labelframe_curves_adhesion = LabelFrame(
            self.Frame_classification, relief=GROOVE, text='Curves of adhesion', width=430)
        self.Labelframe_curves_adhesion.place(
            relx=0.17, rely=0.5, relheight=0.26, relwidth=0.66)

        self.Label_jump_force = Label(
            self.Labelframe_curves_adhesion, text='Jump force (pN)')
        self.Label_jump_force.place(relx=0.14, rely=0.18, height=19, width=91)

        self.Entry_jump_force = Entry(
            self.Labelframe_curves_adhesion, textvariable=self.variable_jump_force, justify='center')
        self.Entry_jump_force.place(
            relx=0.42, rely=0.18, height=19, relwidth=0.34)

        self.Label_jump_position = Label(
            self.Labelframe_curves_adhesion, text='Jump position (nm)')
        self.Label_jump_position.place(
            relx=0.12, rely=0.42, height=19, width=108)

        self.Entry_jump_position = Entry(
            self.Labelframe_curves_adhesion, textvariable=self.variable_jump_position, justify='center')
        self.Entry_jump_position.place(
            relx=0.42, rely=0.42, height=19, relwidth=0.34)

        self.Label_slope_before_jump = Label(
            self.Labelframe_curves_adhesion, text='Slope before jump (-x < slope < +x)')
        self.Label_slope_before_jump.place(
            relx=0.05, rely=0.67, height=19, width=194)

        self.Entry_slope_before_jump = Entry(
            self.Labelframe_curves_adhesion, textvariable=self.variable_slope_before_jump, justify='center')
        self.Entry_slope_before_jump.place(
            relx=0.56, rely=0.67, height=19, relwidth=0.34)

        self.Labelframe_Fit = LabelFrame(
            self.Frame_classification, text='Fit', relief=GROOVE, width=430)
        self.Labelframe_Fit.place(
            relx=0.17, rely=0.81, relheight=0.13, relwidth=0.66)

        self.Label_fit_before_jump = Label(
            self.Labelframe_Fit, text='Fit before jump over (pts)')
        self.Label_fit_before_jump.place(
            relx=0.09, rely=0.4, height=19, width=139)

        self.Entry_fit_before_jump = Entry(
            self.Labelframe_Fit, textvariable=self.variable_fitting_frame, justify='center')
        self.Entry_fit_before_jump.place(
            relx=0.47, rely=0.4, height=19, relwidth=0.34)

        # Creation of the proceed button
        self.Button_proceed = Button(self, text='Proceed', activebackground="#10d919",
                                     relief=GROOVE, command=lambda: Proceed_to_classification())
        self.Button_proceed.place(relx=0.78, rely=0.84, height=37, width=111)


class Supervised_window(Toplevel):
    """Class of the supervised classification window where we can observe
    3 by 3, the pull and press curves and modify/validate the type of event
    and the trace's fit convergence."""

    def __init__(self, *args, **kwargs):
        Toplevel.__init__(self, *args, **kwargs)
        self.focus_set()
        self.initialize()
        self.title("Supervised classification")
        self.height = str(self.winfo_screenwidth())
        self.width = str(self.winfo_screenheight())
        self.geometry(self.height + 'x' + self.width)

    def initialize(self):
        """Contains all the functions and widgets to run this window."""

        # Initialization of the needed variables
        self.cindex = [0, 1, 2]
        self.folder_variable = StringVar(value=folder_path)
        self.variable_goto = StringVar()
        self.variable_yesno1_trace = StringVar()
        self.variable_yesno2_trace = StringVar()
        self.variable_yesno3_trace = StringVar()
        self.variable_retrace1 = StringVar()
        self.variable_retrace2 = StringVar()
        self.variable_retrace3 = StringVar()

        self.variable_total_curves = StringVar()

        self.results = {'Filename': [], 'Date': [], 'Hour': [], 'Condition': [], 'Couple': [], 'Bead': [],
                        'Cell': [], 'Direction': [], 'Sens': [], 'Constant (pN/nm)': [],
                        'Force contact (pN)': [], 'Time break (s)': [], 'Distance (µm)': [],
                        'Speed (µm/s)': [], 'Slope (pN/nm)': [], '[error]': [],
                        'SD baseline retrace (pN)': [], 'Min Force (pN)': [], 'Position Min Force (nm)': [],
                        'Jump force (pN)': [], 'Jump end (nm)': [], 'Pente (pN/nm)': [], 'Tube cassé ?': [],
                        'Aire au min (pN*nm)': [], 'Aire au jump (pN*nm)': [], 'Type of event': [],
                        'Trace\'s fit convergence': [], 'Retrace\'s fit convergence': [], 'Retrace Fitting frame (#pts)': [],
                        'Type of event (corrected)': []}

        output_path = folder_path + '/output-' + str(now)

        self.parameters = model_final.Get_method_parameters(
            outputpath=output_path, now=now)

        self.list_files = model_final.liste_courbes(folder_path)
        self.list_press = []
        self.list_pull = []

        # Test If a method file was loaded by clicking the <Load method file> in the Starting window.
        # And try to retrieve the parameters into a dictionnary (Get_method_parameters)
        try:
            self.parameters_loaded = model_final.Get_method_parameters(
                outputpath=None, now=None, load_path=method_path)
        except NameError:
            pass

        # Try to check if the path in the just written method file is the same as the method file loaded previously in the Starting window.
        # If the paths are same, it open a Yes/No dialogbox by proposing if the user wants to start from the curves which has stopped last time when he worked with these data.
        try:
            if self.parameters['data_folder'] == self.parameters_loaded['data_folder'] and self.parameters_loaded['last_index'] != None:
                self.start_from = messagebox.askyesno("Start from last session", "You have already opened this data folder, do you want to start from "
                                                      + str(self.parameters_loaded['last_index'][0]) + ',' + str(
                                                          self.parameters_loaded['last_index'][1]) + ','
                                                      + str(self.parameters_loaded['last_index'][2]) + ' curves ?')

                if self.start_from == True:
                    self.cindex = self.parameters_loaded['last_index']
                else:
                    pass

        except AttributeError:
            pass

        #---Creation of the Toplevel showing the data processing progression---#

        self.progress = StringVar()
        self.loading_window = Toplevel(self)
        self.Label_loading = Label(self.loading_window, text='Loading data...')
        self.Label_loading.pack(side=TOP)
        self.Label_index = Label(
            self.loading_window, textvariable=self.progress)
        self.Label_index.pack(side=TOP)

        def Supervised_data_processing():
            """This function is similar to the Unsupervised_data_processing function.
            Before begin the supervised classification, we have to load all the needed data.
            For each files contained in the data folder, this function fills the
            list_press and list_pull lists and the results dictionnary.
            """

            for x in self.list_files:
                self.progress.set(str(self.list_files.index(
                    x) + 1) + ' / ' + str(len(self.list_files)))
                self.list_press.append(model_final.Courbe_aller(fichier=x, inputpath=folder_path,
                                                                model=self.parameters['model'], factor=self.parameters['noise_factor'],
                                                                eta=self.parameters['eta'], jump_force=self.parameters['jump_force'],
                                                                jump_position=self.parameters['jump_position'],
                                                                slope_before_jump=self.parameters['slope_before_jump'],
                                                                alignment_epsilon=self.parameters['epsilon'],
                                                                condition=self.parameters['condition'], results=self.results,
                                                                list_press= self.list_press, list_pull= self.list_pull))

                self.list_pull.append(model_final.courbe_retour(fichier=x, inputpath=folder_path,
                                                                model=self.parameters['model'], factor=self.parameters['noise_factor'],
                                                                eta=self.parameters['eta'], jump_force=self.parameters['jump_force'],
                                                                jump_position=self.parameters['jump_position'],
                                                                slope_before_jump=self.parameters['slope_before_jump'],
                                                                alignment_epsilon=self.parameters['epsilon'],
                                                                condition=self.parameters['condition'], results=self.results,
                                                                list_press=self.list_press, pulling_ratio=self.parameters[
                                                                    'pulling_ratio'],
                                                                fitting_frame=self.parameters['fitting_frame']))

                if self.list_pull[-1] == 'NaN':
                    self.list_press.pop(-1)
                    self.list_pull.pop(-1)

            print(len(self.list_press))
            print(len(self.list_pull))

            for y in self.results.keys():
                print(len(self.results[y]), y)

            self.Label_loading.configure(text='Loading is done!')
            counter = {'IN': self.results['Type of event'].count('IN'), 'AL': self.results['Type of event'].count('AL'),
                       'NAd': self.results['Type of event'].count('NAd'), 'AD': self.results['Type of event'].count('AD'), 'TU': self.results['Type of event'].count('TU')}
            self.progress.set('\n' + 'Total of loaded files: ' + str(len(self.list_files)) + '\n\n' + 'IN: ' + str(counter['IN']) + '  AL: ' + str(counter['AL']) +
                              '  NAd: ' + str(counter['NAd']) + '  AD: ' + str(counter['AD']) + '  TU: ' + str(counter['TU']))
            self.variable_total_curves.set('/ ' + str(len(self.list_press)-1))

        class Load_data(Thread):
            """This thread will process all the needed data to perform
            the supervised classification."""

            def __init__(self):
                Thread.__init__(self)

            def run(self):
                Supervised_data_processing()

        Load_data().start()

        def Stop_and_save():
            """Writes the results.txt file and add the index of the last curves visualized at the end of the method.txt file."""

            if self.cindex == [len(self.list_press) - 3, len(self.list_press) - 2, len(self.list_press) - 1]:
                model_final.Output_results_file(
                    path=folder_path, now=now, results=self.results)

            else:
                f = open(output_path + '/' + now + '-method.txt', 'a')
                f.write('# Incomplete classification - last indexes\n')
                f.write(str(self.cindex) + '\n')
                f.close()
                model_final.Output_results_file(
                    path=folder_path, now=now, results=self.results)

        def Display_press_pull_curves(index, list_press, list_pull):
            """ Draw the press and pull curves displayed 3 by 3.

            - the index argument is a list of 3 indexes where each corresponds
            to a curve.txt in the list_press & list_pull.
            - list_press is a list which contains all the names of all jpk.txt to draw the press curves.
            - list_pull contains all the names of all jpk.txt curve to draw the pull curves.

            This function use the dictionnaries returned by the Courbe_aller()
            and courbe_retour() functions from model_final.py script.
            These dictionnaries contains all variables necessary to draw the matplotlib plots.
            """

            # Retrieve the indexes
            x1 = index[0]
            x2 = index[1]
            x3 = index[2]

            # Retrieve the needed dictionnaries of variables to draw the curves
            aller1 = list_press[x1]
            aller2 = list_press[x2]
            aller3 = list_press[x3]
            retour1 = list_pull[x1]
            retour2 = list_pull[x2]
            retour3 = list_pull[x3]

            # Initialization of the matplotlib figure
            fig1 = Figure(dpi=80)
            fig1.set_size_inches(10, 7.5, forward=True)
            fig1.text(0.5, 0.65, aller1['filename'],
                      horizontalalignment='center', style='italic')
            fig1.text(0.5, 0.35, aller2['filename'],
                      horizontalalignment='center', style='italic')
            fig1.text(0.5, 0.05, aller3['filename'],
                      horizontalalignment='center', style='italic')
            a1 = fig1.add_subplot(321)
            a2 = fig1.add_subplot(323)
            a3 = fig1.add_subplot(325)
            b1 = fig1.add_subplot(322)
            b2 = fig1.add_subplot(324)
            b3 = fig1.add_subplot(326)

            #-----------------------press curves initialization-----------------#

            a1.plot(aller1['xcorrige'], aller1['piconewtons'],
                    color='blue', alpha=0.5, label='_nolegend_')
            a1.set_xlabel('TSS (nm)')
            a1.set_ylabel('F (pN)')

            # Test if the fit converged or not. If not, the fit is not drawn and the label 'Fit failed' is added in the legend
            if aller1['fitted'] == 'fit failed':
                handles, labels = a1.get_legend_handles_labels()
                handles.append(mpatches.Patch(color='red', label='Fit failed'))
                a1.legend(handles=handles, bbox_to_anchor=(0.5, 1.2),
                          loc='upper center', ncol=3, fancybox=True)

            else:
                a1.plot(aller1['xcorrige'], aller1['fitted'],
                        color='black', alpha=0.75, label='fit' + aller1['model'])
                a1.legend(bbox_to_anchor=(0.5, 1.2),
                          loc='upper center', ncol=3, fancybox=True)

            a2.plot(aller2['xcorrige'], aller2['piconewtons'],
                    color='blue', alpha=0.5, label='_nolegend_')
            a2.set_xlabel('TSS (nm)')
            a2.set_ylabel('F (pN)')

            if aller2['fitted'] == 'fit failed':
                handles, labels = a2.get_legend_handles_labels()
                handles.append(mpatches.Patch(color='red', label='Fit failed'))
                a2.legend(handles=handles)

            else:
                a2.plot(aller2['xcorrige'], aller2['fitted'],
                        color='black', alpha=0.75, label='fit' + aller2['model'])

            a3.plot(aller3['xcorrige'], aller3['piconewtons'],
                    color='blue', alpha=0.5, label='_nolegend_')
            a3.set_xlabel('TSS (nm)')
            a3.set_ylabel('F (pN)')

            if aller3['fitted'] == 'fit failed':
                handles, labels = a3.get_legend_handles_labels()
                handles.append(mpatches.Patch(color='red', label='Fit failed'))
                a3.legend(handles=handles)

            else:
                a3.plot(aller3['xcorrige'], aller3['fitted'],
                        color='black', alpha=0.75, label='fit' + aller3['model'])

            #------------------------------------------------------------------#

            #----------------------Pull curves initialization------------------#

            # Test if it's a non-adhesive event, if it's true no fit is drawn
            if retour1['fini'] == 'NoAdh':
                b1.plot(retour1['xcorrige'], retour1['piconewtons'],
                        color='green', alpha=0.5, label='_nolegend_')
                b1.set_xlabel('TSS (nm)')
                b1.set_ylabel('F (pN)')

                b1.scatter(retour1['minimumposition'],
                           retour1['minimum'], color='red', label='minimum')
                b1.scatter(retour1['minimumvshapeposition'],
                           retour1['minimumvshape'], color='black', label='zero')
                b1.axhline(y=-(retour1['overcoming']), alpha=0.5, ls='-.', label=str(
                    retour1['factor']) + ' x SD = '+str(round(retour1['overcoming'], 1))+'pN')
                b1.axhline(y=0, alpha=0.25, ls='-.')
                b1.legend(bbox_to_anchor=(0.5, 1.3),
                          loc='upper center', ncol=3, fancybox=True)

            else:
                b1.plot(retour1['xcorrige'], retour1['piconewtons'],
                        color='green', alpha=0.5, label='_nolegend_')
                b1.set_xlabel('TSS (nm)')
                b1.set_ylabel('F (pN)')

                b1.scatter(retour1['minimumposition'],
                           retour1['minimum'], color='red', label='minimum')
                b1.scatter(retour1['minimumvshapeposition'],
                           retour1['minimumvshape'], color='black', label='zero')
                b1.axhline(y=-(retour1['overcoming']), alpha=0.5, ls='-.', label=str(
                    retour1['factor']) + ' x SD = '+str(round(retour1['overcoming'], 1))+'pN')
                b1.axhline(y=0, alpha=0.25, ls='-.')

                b1.scatter(retour1['xcorrige[pointfin]'], retour1['piconewtons[pointfin]'],
                           color='green', alpha=1, label='fin saut')
                b1.scatter(retour1['positionjump'], retour1['jumpheight'],
                           color='blue', alpha=1, label='jump')

                if retour1['fitted'] == 'fit failed':
                    handles, labels = b1.get_legend_handles_labels()
                    handles.append(mpatches.Patch(
                        color='red', label='Fit failed'))
                    b1.legend(handles=handles, bbox_to_anchor=(
                        0.5, 1.3), loc='upper center', ncol=3, fancybox=True)

                else:
                    b1.plot(retour1['xzonefit'], retour1['fitted'],
                            color='black', alpha=0.75, label='fit' + retour1['model'])
                    b1.legend(bbox_to_anchor=(0.5, 1.3),
                              loc='upper center', ncol=3, fancybox=True)

            if retour2['fini'] == 'NoAdh':
                b2.plot(retour2['xcorrige'], retour2['piconewtons'],
                        color='green', alpha=0.5, label='_nolegend_')
                b2.set_xlabel('TSS (nm)')
                b2.set_ylabel('F (pN)')

                b2.scatter(
                    retour2['minimumposition'], retour2['minimum'], color='red', label='_nolegend_')
                b2.scatter(retour2['minimumvshapeposition'],
                           retour2['minimumvshape'], color='black', label='_nolegend_')
                b2.axhline(y=-(retour2['overcoming']), alpha=0.5, ls='-.', label=str(
                    retour2['factor']) + ' x SD = '+str(round(retour2['overcoming'], 1))+'pN')
                b2.axhline(y=0, alpha=0.25, ls='-.')
                b2.legend()

            else:
                b2.plot(retour2['xcorrige'], retour2['piconewtons'],
                        color='green', alpha=0.5, label='_nolegend_')
                b2.set_xlabel('TSS (nm)')
                b2.set_ylabel('F (pN)')

                b2.scatter(
                    retour2['minimumposition'], retour2['minimum'], color='red', label='_nolegend_')
                b2.scatter(retour2['minimumvshapeposition'],
                           retour2['minimumvshape'], color='black', label='_nolegend_')
                b2.axhline(y=-(retour2['overcoming']), alpha=0.5, ls='-.', label=str(
                    retour2['factor']) + ' x SD = '+str(round(retour2['overcoming'], 1))+'pN')
                b2.axhline(y=0, alpha=0.25, ls='-.')

                b2.scatter(retour2['xcorrige[pointfin]'], retour2['piconewtons[pointfin]'],
                           color='green', alpha=1, label='_nolegend_')
                b2.scatter(retour2['positionjump'], retour2['jumpheight'],
                           color='blue', alpha=1, label='_nolegend_')

                if retour2['fitted'] == 'fit failed':
                    handles, labels = b2.get_legend_handles_labels()
                    handles.append(mpatches.Patch(
                        color='red', label='Fit failed'))
                    b2.legend(handles=handles)

                else:
                    b2.plot(retour2['xzonefit'], retour2['fitted'],
                            color='black', alpha=0.75, label='_nolegend_')
                    b2.legend()

            if retour3['fini'] == 'NoAdh':
                b3.plot(retour3['xcorrige'], retour3['piconewtons'],
                        color='green', alpha=0.5, label='_nolegend_')
                b3.set_xlabel('TSS (nm)')
                b3.set_ylabel('F (pN)')

                b3.scatter(
                    retour3['minimumposition'], retour3['minimum'], color='red', label='_nolegend_')
                b3.scatter(retour3['minimumvshapeposition'],
                           retour3['minimumvshape'], color='black', label='_nolegend_')
                b3.axhline(y=-(retour3['overcoming']), alpha=0.5, ls='-.', label=str(
                    retour3['factor']) + ' x SD = '+str(round(retour3['overcoming'], 1))+'pN')
                b3.axhline(y=0, alpha=0.25, ls='-.')
                b3.legend()

            else:
                b3.plot(retour3['xcorrige'], retour3['piconewtons'],
                        color='green', alpha=0.5, label='_nolegend_')
                b3.set_xlabel('TSS (nm)')
                b3.set_ylabel('F (pN)')

                b3.scatter(
                    retour3['minimumposition'], retour3['minimum'], color='red', label='_nolegend_')
                b3.scatter(retour3['minimumvshapeposition'],
                           retour3['minimumvshape'], color='black', label='_nolegend_')
                b3.axhline(y=-(retour3['overcoming']), alpha=0.5, ls='-.', label=str(
                    retour3['factor']) + ' x SD = '+str(round(retour3['overcoming'], 1))+'pN')
                b3.axhline(y=0, alpha=0.25, ls='-.')

                b3.scatter(retour3['xcorrige[pointfin]'], retour3['piconewtons[pointfin]'],
                           color='green', alpha=1, label='_nolegend_')
                b3.scatter(retour3['positionjump'], retour3['jumpheight'],
                           color='blue', alpha=1, label='_nolegend_')

                if retour3['fitted'] == 'fit failed':
                    handles, labels = b3.get_legend_handles_labels()
                    handles.append(mpatches.Patch(
                        color='red', label='Fit failed'))
                    b3.legend(handles=handles)

                else:
                    b3.plot(retour3['xzonefit'], retour3['fitted'],
                            color='black', alpha=0.75, label='_nolegend_')
                    b3.legend()

            fig1.tight_layout(pad=2.5, h_pad=1)

            #------------------------------------------------------------------#

            # Creation of the matplotlib's canvas which will contain the figure
            self.canvas_fig = FigureCanvasTkAgg(
                fig1, master=self.Frame_courbes)
            self.canvas_fig.show()
            self.canvas_fig.get_tk_widget().pack(expand=True, fill=BOTH)

            # Creation of the scrollbar attached to the Frame=self.Frame_courbes.
            self.canvas_fig.get_tk_widget().config(
                yscrollcommand=self.yscrollbar_courbes.set)
            self.yscrollbar_courbes.config(
                command=self.canvas_fig.get_tk_widget().yview)

            # Creation of the Frame for the matplotlib's toolbar
            self.toolbar_frame = Frame(self)
            self.toolbar_frame.place(
                relx=0.01, rely=0.9, relheight=0.06, relwidth=0.61)

            # matplotlib's toolbar creation
            self.toolbar = NavigationToolbar2TkAgg(
                self.canvas_fig, self.toolbar_frame)
            self.toolbar.update()

            # Creation of the function which links the matplotlib figure and toolbar functionalities
            def on_key_event(event):
                print('you pressed %s' % event.key)
                key_press_handler(event, self.canvas_fig, self.toolbar)

            self.canvas_fig.mpl_connect('key_press_event', on_key_event)

        def next_previous(seq):
            """ Change the current indexes (self.cindex = [x1, x2, x3]) according to the clicked button
            (next, previous).

            - Add (if seq argument == 'next') or substract (if seq argument == 'previous')
             the list of indexes (= self.cindex) by 3.

            - Once the self.cindex modified, the press/pull curves are drawn again
            and values in the Entry= self.Entry_index are updated.

            - Displays and record the modifications about the Trace's fit convergence (yes/no) and the class of events (R, NA, AD, TU).
            """

            listcourbes = self.list_files

            t, s = divmod(len(listcourbes), 3)

            #---------------For the <Start press/pull curves> button-----------#
            if seq == 'zero':
                Display_press_pull_curves(
                    self.cindex, self.list_press, self.list_pull)

                # Display for each file, the values about results[Trace's fit convergence] and results[Type of event (corrected)] contained in the <results> dict
                # Before that, we have to retrieve for these 3 files the index corresponding to the <results> dict from the list_pull or list_press
                curve1 = self.list_pull[self.cindex[0]]
                curve2 = self.list_pull[self.cindex[1]]
                curve3 = self.list_pull[self.cindex[2]]
                name_curve1 = curve1['filename']
                name_curve2 = curve2['filename']
                name_curve3 = curve3['filename']

                # Check if the file names correspond to the names displayed in the matplotlib figure
                # print(name_curve1)
                # print(name_curve2)
                # print(name_curve3)

                self.__class__.index_curve1 = self.list_files.index(
                    name_curve1)
                self.__class__.index_curve2 = self.list_files.index(
                    name_curve2)
                self.__class__.index_curve3 = self.list_files.index(
                    name_curve3)

                self.variable_retrace1.set(
                    self.results['Type of event (corrected)'][self.index_curve1])
                self.variable_retrace2.set(
                    self.results['Type of event (corrected)'][self.index_curve2])
                self.variable_retrace3.set(
                    self.results['Type of event (corrected)'][self.index_curve3])
                self.variable_yesno1_trace.set(
                    str(self.results['Trace\'s fit convergence'][self.index_curve1]))
                self.variable_yesno2_trace.set(
                    str(self.results['Trace\'s fit convergence'][self.index_curve2]))
                self.variable_yesno3_trace.set(
                    str(self.results['Trace\'s fit convergence'][self.index_curve3]))

                # Deactivate the <Start press/pull curves> button to avoid a bug in the matplotlib figure when whe click a second time on the button.
                self.Button_start_curves.configure(state='disabled')
                #--------------------------------------------------------------#

            #------------------------For the <Next> button---------------------#
            if seq == 'next':
                if self.cindex[-1] == len(self.list_press) - 1:
                    pass

                # We record all modifications on class events done by the user. By updating the corresponding results['Type of event (corrected)'] value.
                else:
                    self.results['Type of event (corrected)'][self.index_curve1] = self.variable_retrace1.get(
                    )
                    self.results['Type of event (corrected)'][self.index_curve2] = self.variable_retrace2.get(
                    )
                    self.results['Type of event (corrected)'][self.index_curve3] = self.variable_retrace3.get(
                    )

                    # If the Trace's fit convergence is not validated by the user, we update the corresponing results['Trace's fit convergence] value to 'False' and the results[Slope (pN/nm)] value to 'NaN'.
                    if self.variable_yesno1_trace.get() == 'False':
                        self.results['Trace\'s fit convergence'][self.index_curve1] = False
                        self.results['Slope (pN/nm)'][self.index_curve1] = 'NaN'

                    if self.variable_yesno2_trace.get() == 'False':
                        self.results['Trace\'s fit convergence'][self.index_curve2] = False
                        self.results['Slope (pN/nm)'][self.index_curve2] = 'NaN'

                    if self.variable_yesno3_trace.get() == 'False':
                        self.results['Trace\'s fit convergence'][self.index_curve3] = False
                        self.results['Slope (pN/nm)'][self.index_curve3] = 'NaN'

                    self.cindex = list(map(add, self.cindex, [3, 3, 3]))

                    if self.cindex[0] == t*3:
                        self.cindex = [
                            len(self.list_press) - 3, len(self.list_press) - 2, len(self.list_press) - 1]

                        # Before displaying the next figure we have to destroy the current one
                        self.canvas_fig.get_tk_widget().destroy()
                        Display_press_pull_curves(
                            self.cindex, self.list_press, self.list_pull)

                        curve1 = self.list_pull[self.cindex[0]]
                        curve2 = self.list_pull[self.cindex[1]]
                        curve3 = self.list_pull[self.cindex[2]]
                        name_curve1 = curve1['filename']
                        name_curve2 = curve2['filename']
                        name_curve3 = curve3['filename']

                        # print(name_curve1)
                        # print(name_curve2)
                        # print(name_curve3)

                        self.index_curve1 = self.list_files.index(name_curve1)
                        self.index_curve2 = self.list_files.index(name_curve2)
                        self.index_curve3 = self.list_files.index(name_curve3)

                        self.variable_retrace1.set(
                            self.results['Type of event (corrected)'][self.index_curve1])
                        self.variable_retrace2.set(
                            self.results['Type of event (corrected)'][self.index_curve2])
                        self.variable_retrace3.set(
                            self.results['Type of event (corrected)'][self.index_curve3])
                        self.variable_yesno1_trace.set(
                            str(self.results['Trace\'s fit convergence'][self.index_curve1]))
                        self.variable_yesno2_trace.set(
                            str(self.results['Trace\'s fit convergence'][self.index_curve2]))
                        self.variable_yesno3_trace.set(
                            str(self.results['Trace\'s fit convergence'][self.index_curve3]))

                    else:
                        self.canvas_fig.get_tk_widget().destroy()
                        Display_press_pull_curves(
                            self.cindex, self.list_press, self.list_pull)

                        curve1 = self.list_pull[self.cindex[0]]
                        curve2 = self.list_pull[self.cindex[1]]
                        curve3 = self.list_pull[self.cindex[2]]
                        name_curve1 = curve1['filename']
                        name_curve2 = curve2['filename']
                        name_curve3 = curve3['filename']

                        # print(name_curve1)
                        # print(name_curve2)
                        # print(name_curve3)

                        self.index_curve1 = self.list_files.index(name_curve1)
                        self.index_curve2 = self.list_files.index(name_curve2)
                        self.index_curve3 = self.list_files.index(name_curve3)

                        self.variable_retrace1.set(
                            self.results['Type of event (corrected)'][self.index_curve1])
                        self.variable_retrace2.set(
                            self.results['Type of event (corrected)'][self.index_curve2])
                        self.variable_retrace3.set(
                            self.results['Type of event (corrected)'][self.index_curve3])
                        self.variable_yesno1_trace.set(
                            str(self.results['Trace\'s fit convergence'][self.index_curve1]))
                        self.variable_yesno2_trace.set(
                            str(self.results['Trace\'s fit convergence'][self.index_curve2]))
                        self.variable_yesno3_trace.set(
                            str(self.results['Trace\'s fit convergence'][self.index_curve3]))
            #------------------------------------------------------------------#

            #----------------------For the <Previous> button-------------------#
            if seq == 'prev':
                if self.cindex == [0, 1, 2]:
                    pass

                else:
                    self.cindex = list(map(sub, self.cindex, [3, 3, 3]))
                    self.canvas_fig.get_tk_widget().destroy()
                    Display_press_pull_curves(
                        self.cindex, self.list_press, self.list_pull)

                    curve1 = self.list_pull[self.cindex[0]]
                    curve2 = self.list_pull[self.cindex[1]]
                    curve3 = self.list_pull[self.cindex[2]]
                    name_curve1 = curve1['filename']
                    name_curve2 = curve2['filename']
                    name_curve3 = curve3['filename']

                    # print(name_curve1)
                    # print(name_curve2)
                    # print(name_curve3)

                    self.index_curve1 = self.list_files.index(name_curve1)
                    self.index_curve2 = self.list_files.index(name_curve2)
                    self.index_curve3 = self.list_files.index(name_curve3)

                    self.variable_retrace1.set(
                        self.results['Type of event (corrected)'][self.index_curve1])
                    self.variable_retrace2.set(
                        self.results['Type of event (corrected)'][self.index_curve2])
                    self.variable_retrace3.set(
                        self.results['Type of event (corrected)'][self.index_curve3])
                    self.variable_yesno1_trace.set(
                        str(self.results['Trace\'s fit convergence'][self.index_curve1]))
                    self.variable_yesno2_trace.set(
                        str(self.results['Trace\'s fit convergence'][self.index_curve2]))
                    self.variable_yesno3_trace.set(
                        str(self.results['Trace\'s fit convergence'][self.index_curve3]))
            #------------------------------------------------------------------#
            # Update of the Entry_index
            self.Entry_index.delete(0, END)
            self.Entry_index.insert(
                0, str(self.cindex[0])+','+str(self.cindex[1])+','+str(self.cindex[2]))

        def Go_to():
            """Retrieve the indexes inserted in the Entry= self.Entry_index and
            draw the curves according to these indexes."""

            new_index = self.variable_goto.get()
            new_index = new_index.split(',')
            new_index = [int(n) for n in new_index]

            self.canvas_fig.get_tk_widget().destroy()
            Display_press_pull_curves(
                new_index, self.list_press, self.list_pull)

            curve1 = self.list_pull[new_index[0]]
            curve2 = self.list_pull[new_index[1]]
            curve3 = self.list_pull[new_index[2]]
            name_curve1 = curve1['filename']
            name_curve2 = curve2['filename']
            name_curve3 = curve3['filename']

            # print(name_curve1)
            # print(name_curve2)
            # print(name_curve3)

            self.index_curve1 = self.list_files.index(name_curve1)
            self.index_curve2 = self.list_files.index(name_curve2)
            self.index_curve3 = self.list_files.index(name_curve3)

            self.variable_retrace1.set(
                self.results['Type of event (corrected)'][self.index_curve1])
            self.variable_retrace2.set(
                self.results['Type of event (corrected)'][self.index_curve2])
            self.variable_retrace3.set(
                self.results['Type of event (corrected)'][self.index_curve3])
            self.variable_yesno1_trace.set(
                str(self.results['Trace\'s fit convergence'][self.index_curve1]))
            self.variable_yesno2_trace.set(
                str(self.results['Trace\'s fit convergence'][self.index_curve2]))
            self.variable_yesno3_trace.set(
                str(self.results['Trace\'s fit convergence'][self.index_curve3]))

        #-----------------Creation of the main frames--------------------------#
        self.Frame_courbes = Frame(
            self, borderwidth='2', relief=RIDGE, width=861)
        self.Frame_courbes.place(relx=0.01, rely=0.02,
                                 relheight=0.86, relwidth=0.58)

        self.Frame_folder_selected = Frame(
            self, borderwidth='2', relief=GROOVE, width=525)
        self.Frame_folder_selected.place(
            relx=0.62, rely=0.02, relheight=0.1, relwidth=0.37)

        self.Frame_keep_trace = Frame(
            self, borderwidth='2', relief=GROOVE, width=215)
        self.Frame_keep_trace.place(
            relx=0.62, rely=0.24, relheight=0.26, relwidth=0.15)

        self.Frame_keep_retrace = Frame(
            self, borderwidth='2', relief=GROOVE, width=225)
        self.Frame_keep_retrace.place(
            relx=0.78, rely=0.24, relheight=0.26, relwidth=0.2)

        self.Frame_scrollbar_folder = Frame(
            self.Frame_folder_selected, borderwidth='2', relief=FLAT, width=405)
        self.Frame_scrollbar_folder.place(
            relx=0.21, rely=0.59, relheight=0.29, relwidth=0.77)

        self.Frame_scrollbar_curves = Frame(self)
        self.Frame_scrollbar_curves.place(
            relx=0.59, rely=0.02, relheight=0.86, relwidth=0.02)

        #----------------------------------------------------------------------#

        #---------------------------Creation of the buttons--------------------#

        # Creation of the label "folder selected:" located in the Frame= self.Frame_folder_selected
        self.folder_selected_label = Label(
            self.Frame_folder_selected, text='Folder selected:')
        self.folder_selected_label.place(
            relx=0.02, rely=0.18, height=19, width=100)

        # Creation of the Text widget which contains the path of the selected folder
        self.folder_selected_text = Text(
            self.Frame_folder_selected, wrap='none', width=406)
        self.folder_selected_text.place(
            relx=0.21, rely=0.12, relheight=0.42, relwidth=0.77)

        self.folder_selected_text.delete('1.0', END)
        self.folder_selected_text.insert('1.0', self.folder_variable.get())

        # Creation of the scrollbar for the Frame= self.Frame_folder_selected
        self.scrollbar_folder_selected_frame = Scrollbar(
            self.Frame_scrollbar_folder, orient='horizontal')
        self.scrollbar_folder_selected_frame.pack(fill=X, expand=True)
        self.folder_selected_text.configure(
            xscrollcommand=self.scrollbar_folder_selected_frame.set, font=('Helvetica', 8))
        self.scrollbar_folder_selected_frame.config(
            command=self.folder_selected_text.xview)

        self.yscrollbar_courbes = Scrollbar(self.Frame_scrollbar_curves)
        self.yscrollbar_courbes.pack(expand=True, fill=Y, side=RIGHT)

        # Creation of the "Start press/pull curves" button
        self.Button_start_curves = Button(self, text="Start press/pull curves", command=lambda: next_previous(
            'zero'), activebackground="#11ff00", font=('Helvetica', 10, 'bold'))
        self.Button_start_curves.place(
            relx=0.68, rely=0.16, height=38, width=166)

        # Creation of the "Stop & save" button
        self.Button_stop_curves = Button(self, text='Stop & Save', command=lambda: Stop_and_save(
        ), activebackground="#d9082b", font=('Helvetica', 10, 'bold'))
        self.Button_stop_curves.place(
            relx=0.82, rely=0.16, height=38, width=102)

        # Creation of the "Next images button"
        self.next_button = Button(
            self, text="Next curves", command=lambda: next_previous('next'))
        self.next_button.place(relx=0.81, rely=0.53, height=27, width=90)

        # Creation of the "Previous" image button
        self.previous_button = Button(
            self, text="Previous curves", command=lambda: next_previous('prev'))
        self.previous_button.place(relx=0.72, rely=0.53, height=27, width=113)
        self.previous_button.configure(width=101)

        # Creation of the Entry which are displayed the indexes
        self.Entry_index = Entry(self, textvariable=self.variable_goto, font=(
            'Helvetica', 10, 'bold'), width=86)
        self.Entry_index.place(relx=0.74, rely=0.62, height=39, relwidth=0.07)

        # Creation of the label which shows the total number of curves
        self.Label_total_courbes = Label(
            self, textvariable=self.variable_total_curves, font=('Helvetica', 12, 'bold'))
        self.Label_total_courbes.place(
            relx=0.82, rely=0.64, height=19, width=40)

        # Creation of the "Go to" button
        self.Goto_button = Button(self, text='Go to', command=Go_to)
        self.Goto_button.place(relx=0.78, rely=0.69, height=27, width=55)

        # Creatio of the "Draw scatter plots" button
        self.Button_Draw_scatter_plots = Button(self, text='Draw scatter plots', command=lambda: model_final.Draw_scatter_slope(
            results=self.results, noise_factor=self.parameters['noise_factor'], slope_before_jump=self.parameters['slope_before_jump'], jump_position=self.parameters['jump_position']))
        self.Button_Draw_scatter_plots.place(
            relx=0.76, rely=0.75, height=37, width=130)

        # Creation of the labels in the Frame= Frame_keep_trace
        self.Label_keep_trace = Label(
            self.Frame_keep_trace, text='Keep trace\'s fit ?', font=('Helvetica', 11, 'bold'))
        self.Label_keep_trace.place(relx=0.23, rely=0.05, height=22, width=122)

        self.Label_curve1_trace = Label(self.Frame_keep_trace, text='Curve 1')
        self.Label_curve1_trace.place(
            relx=0.09, rely=0.28, height=19, width=47)

        self.Label_curve2_trace = Label(self.Frame_keep_trace, text='Curve 2')
        self.Label_curve2_trace.place(
            relx=0.09, rely=0.51, height=19, width=50)

        self.Label_curve3_trace = Label(self.Frame_keep_trace, text='Curve 3')
        self.Label_curve3_trace.place(
            relx=0.09, rely=0.74, height=19, width=47)

        # Creation of the labels in the Frame = Frame_keep_retrace
        self.Label_keep_retrace = Label(
            self.Frame_keep_retrace, text='Choose a class', font=('Helvetica', 11, 'bold'))
        self.Label_keep_retrace.place(
            relx=0.32, rely=0.05, height=22, width=110)

        self.Label_curve1_retrace = Label(
            self.Frame_keep_retrace, text='Curve 1')
        self.Label_curve1_retrace.place(
            relx=0.09, rely=0.28, height=19, width=47)

        self.Label_curve2_retrace = Label(
            self.Frame_keep_retrace, text='Curve 2')
        self.Label_curve2_retrace.place(
            relx=0.09, rely=0.51, height=19, width=47)

        self.Label_curve3_retrace = Label(
            self.Frame_keep_retrace, text='Curve 3')
        self.Label_curve3_retrace.place(
            relx=0.09, rely=0.74, height=19, width=47)

        # Creation of the radiobuttons yes/no for the Frame = Frame_keep_retrace
        self.Radiobutton_retrace1_R = Radiobutton(
            self.Frame_keep_retrace, text='R', variable=self.variable_retrace1, value='R')
        self.Radiobutton_retrace1_R.place(
            relx=0.28, rely=0.28, relheight=0.1, relwidth=0.16)

        self.Radiobutton_retrace2_R = Radiobutton(
            self.Frame_keep_retrace, text='R', variable=self.variable_retrace2, value='R')
        self.Radiobutton_retrace2_R.place(
            relx=0.28, rely=0.51, relheight=0.1, relwidth=0.16)

        self.Radiobutton_retrace3_R = Radiobutton(
            self.Frame_keep_retrace, text='R', variable=self.variable_retrace3, value='R')
        self.Radiobutton_retrace3_R.place(
            relx=0.28, rely=0.74, relheight=0.1, relwidth=0.16)

        self.Radiobutton_retrace1_NA = Radiobutton(
            self.Frame_keep_retrace, text='NAd', variable=self.variable_retrace1, value='NAd')
        self.Radiobutton_retrace1_NA.place(
            relx=0.46, rely=0.28, relheight=0.1, relwidth=0.16)

        self.Radiobutton_retrace2_NA = Radiobutton(
            self.Frame_keep_retrace, text='NAd', variable=self.variable_retrace2, value='NAd')
        self.Radiobutton_retrace2_NA.place(
            relx=0.46, rely=0.51, relheight=0.1, relwidth=0.16)

        self.Radiobutton_retrace3_NA = Radiobutton(
            self.Frame_keep_retrace, text='NAd', variable=self.variable_retrace3, value='NAd')
        self.Radiobutton_retrace3_NA.place(
            relx=0.46, rely=0.74, relheight=0.1, relwidth=0.16)

        self.Radiobutton_retrace1_AD = Radiobutton(
            self.Frame_keep_retrace, text='AD', variable=self.variable_retrace1, value='AD')
        self.Radiobutton_retrace1_AD.place(
            relx=0.63, rely=0.28, relheight=0.1, relwidth=0.15)

        self.Radiobutton_retrace2_AD = Radiobutton(
            self.Frame_keep_retrace, text='AD', variable=self.variable_retrace2, value='AD')
        self.Radiobutton_retrace2_AD.place(
            relx=0.63, rely=0.51, relheight=0.1, relwidth=0.15)

        self.Radiobutton_retrace3_AD = Radiobutton(
            self.Frame_keep_retrace, text='AD', variable=self.variable_retrace3, value='AD')
        self.Radiobutton_retrace3_AD.place(
            relx=0.63, rely=0.74, relheight=0.1, relwidth=0.15)

        self.Radiobutton_retrace1_TU = Radiobutton(
            self.Frame_keep_retrace, text='TU', variable=self.variable_retrace1, value='TU')
        self.Radiobutton_retrace1_TU.place(
            relx=0.81, rely=0.28, relheight=0.1, relwidth=0.15)

        self.Radiobutton_retrace2_TU = Radiobutton(
            self.Frame_keep_retrace, text='TU', variable=self.variable_retrace2, value='TU')
        self.Radiobutton_retrace2_TU.place(
            relx=0.81, rely=0.51, relheight=0.1, relwidth=0.15)

        self.Radiobutton_retrace3_TU = Radiobutton(
            self.Frame_keep_retrace, text='TU', variable=self.variable_retrace3, value='TU')
        self.Radiobutton_retrace3_TU.place(
            relx=0.81, rely=0.74, relheight=0.1, relwidth=0.15)

        # Creation of the radiobuttons yes/no for the Frame = Frame_keep_trace
        self.Radiobutton_yes1_trace = Radiobutton(
            self.Frame_keep_trace, text='yes', variable=self.variable_yesno1_trace, value='True', justify=LEFT, activebackground="#d9d9d9")
        self.Radiobutton_yes1_trace.place(
            relx=0.42, rely=0.28, relheight=0.1, relwidth=0.22)

        self.Radiobutton_yes2_trace = Radiobutton(
            self.Frame_keep_trace, text='yes', variable=self.variable_yesno2_trace, value='True', justify=LEFT, activebackground="#d9d9d9")
        self.Radiobutton_yes2_trace.place(
            relx=0.42, rely=0.51, relheight=0.1, relwidth=0.22)

        self.Radiobutton_yes3_trace = Radiobutton(
            self.Frame_keep_trace, text='yes', variable=self.variable_yesno3_trace, value='True', justify=LEFT, activebackground="#d9d9d9")
        self.Radiobutton_yes3_trace.place(
            relx=0.42, rely=0.74, relheight=0.1, relwidth=0.22)

        self.Radiobutton_no1_trace = Radiobutton(
            self.Frame_keep_trace, text='no', variable=self.variable_yesno1_trace, value='False', justify=LEFT, activebackground="#d9d9d9")
        self.Radiobutton_no1_trace.place(
            relx=0.65, rely=0.28, relheight=0.1, relwidth=0.2)

        self.Radiobutton_no2_trace = Radiobutton(
            self.Frame_keep_trace, text='no', variable=self.variable_yesno2_trace, value='False', justify=LEFT, activebackground="#d9d9d9")
        self.Radiobutton_no2_trace.place(
            relx=0.65, rely=0.51, relheight=0.1, relwidth=0.2)

        self.Radiobutton_no3_trace = Radiobutton(
            self.Frame_keep_trace, text='no', variable=self.variable_yesno3_trace, value='False', justify=LEFT, activebackground="#ff7809")
        self.Radiobutton_no3_trace.place(
            relx=0.65, rely=0.74, relheight=0.1, relwidth=0.2)


if __name__ == "__main__":
    app = Start_window(None)
    app.title("Force data classifier")
    app.mainloop()
